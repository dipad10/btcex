﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="faq.aspx.vb" Inherits="faq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/Header.ascx" TagName="n2theader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/footer.ascx" TagName="n2tfooter" TagPrefix="uc2" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>BtcEx</title>

    <meta name="keywords" content="Exchange bitcoin, bitcoin exchange, nairaex, Exchnage, btc, Ex, btcex, change yourbitcoin, bitcoin, blockchain" />
    <meta name="description" content="Nigerias No1. Bitcoin Exchange site">
    <meta name="author" content="BtcEX">
      <meta name="theme-color" content="#4dbb6d" />
    <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/favicon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CSintony:400,700" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-business-consulting.css">
    <script src="master/style-switcher/style.switcher.localstorage.js"></script>

    <!-- Demo CSS -->
    <link rel="stylesheet" href="css/demos/demo-business-consulting.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>


</head>
<body>
   <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type="text/javascript">window.$crisp = []; window.CRISP_WEBSITE_ID = "a9b456e9-59ec-456b-b180-51b4cce1f8c8"; (function () { d = document; s = d.createElement("script"); s.src = "https://client.crisp.im/l.js"; s.async = 1; d.getElementsByTagName("head")[0].appendChild(s); })();</script>
    <!-- {/literal} END JIVOSITE CODE -->
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="body">
            <uc1:n2theader ID="n2theader" runat="server" />
            <div role="main" class="main">

                <section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>- FAQ <span>Frequently Asked Questions</span></h1>
                                <ul class="breadcrumb breadcrumb-valign-mid">
                                    <li><a href="demo-business-consulting.html">Home</a></li>
                                    <li class="active">FAQ</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                
				<div class="container">

					<h2>Frequently Asked <strong>Questions</strong></h2>

					<div class="row">
						<div class="col-md-12">
							<p class="lead small">
								Frequently asked questions from Visitors. Hope you find the answer to your Question here.
							</p>
						</div>
					</div>

					<hr>

					<div class="row">
						<div class="col-md-12">

							<div class="toggle toggle-primary" data-plugin-toggle="">
								<section class="toggle active">
									<label>How to Buy Bitcoins on btcex.com.ng</label>
									<p class="preview-active">
                                        Register at btcex.com.ng, verify your account by opening your profile page and upload your valid government ID e.g(Driver's licence, Naitional ID card, Voters Card. Etc)<br />
Login into your account, and click create new order below the active order on the dashboard and select buy -<br />
Fill out the amount and enter where you want us to send the currency to.<br />
Make the deposit to the bank account on the order page and marked as complete after deposit. Upload the proof of payment (teller, payslip, statement or transfer screenshot etc) on the portal and your order will be funded within 2-8 hours after your deposit has been verified.<br />
									</p>
								</section>

							

								<section class="toggle">
									<label>How to Sell Bitcoins on btcex.com.ng</label>
									<p>
                                    Register at btcex.com.ng. Account verification not needed for sell orders.<br />
Login into your account, click on your name on the right side and select setup paymentInfo in order to add your bank account details.<br />
Go to the sell orders and click on the create new order
Select sell and the system will show you the bank account you've setup before and fill out the amount.
Send the e-currency to the bitcoin address. Your naira will be deposited in your account within 2-8 hours.   
                                    </p>
								</section>

								<section class="toggle">
									<label>How to complete NairaEx account verification</label>
									<p>
                                       Go to your profile on the portal and enter your phone number and address. Upload government issued ID card like driver's license, voters card, intl passport etc on the portal and submit.
									</p>
								</section>

                                	<section class="toggle">
									<label>What is bitcoin?</label>
									<p>
                                      Bitcoin is a digital currency. You can use Bitcoin to send money to anyone via the Internet with no middleman. Learn more here.
									</p>
								</section>
                                <section class="toggle">
									<label>How to start using Bitcoin?</label>
									<p>
                                    Before you will be able to buy or sell your Bitcoins you will need to create a Bitcoin wallet. Bitcoin wallet is similar to bank account where you can generate a bitcoin address that serve as a bank account number that can be given out to other users in order to receive and send bitcoin. An example of a Bitcoin address: 1or96uGo5F7yGqETzpTmuJzvVq49eRBDi<br />
                                        Blockchain.info is the most widely used Bitcoin wallet today, it allows you to send and receive Bitcoins through your wallet from your browser or mobile phone. You can create a bitcoin wallet at https://blockchain.info/wallet
                                        <br />
                                        The Complete Guide To Creating, receiving and sending Bitcoins Wallet can be found <a href="http://btcex.com.ng/aboutbitcoin">Here</a>
									</p>
								</section>

                                  
							</div>

						</div>

					</div>

				</div>






                <%-- insert footer --%>
                <section class="section section-text-light section-background m-none" style="background: url('img/demos/business-consulting/contact/contact-background.jpg'); background-size: cover;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="font-weight-bold">- Contact Us</h2>
                                <div class="col-md-6 pl-none">
                                    <h4 class="mb-xs">Call Us</h4>
                                    <a href="tel:+2348166435654" class="text-decoration-none" target="_blank" title="Call Us">
                                        <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">Phone
										
                                            <span class="info text-lg">+2348166435654
											</span>
                                        </span>
                                    </a>
                                </div>

                                <div class="col-md-6 pl-none">
                                    <h4 class="mb-xs">Mail Us</h4>
                                    <a href="mail:mail@example.com" class="text-decoration-none" target="_blank" title="Mail Us">
                                        <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">Email
										
                                            <span class="info text-lg">support@btcex.com.ng
											</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="col-md-6 pl-none custom-sm-margin-top">
                                    <h4 class="mb-xs">Social Media</h4>
                                    <ul class="social-icons custom-social-icons-style-1 custom-opacity-font">
                                        <li class="social-icons-facebook">
                                            <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-twitter">
                                            <a href="http://www.twitter.com/" target="_blank" title="Twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-instagram">
                                            <a href="http://www.instagram.com/" target="_blank" title="Instagram">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-linkedin">
                                            <a href="http://www.linkedin.com/" target="_blank" title="Linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 custom-sm-margin-top">
                                <h2 class="font-weight-bold">-Join Us Today</h2>
                                <p class="lead">Now well over 150,000 registered members Worldwide!</p>
                                <p>
                                    <a class="btn btn-borders custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" href="/private/register">Sign Up Today</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <%-- insertfii --%>
            <uc2:n2tfooter ID="n2tfooter" runat="server" />
        </div>

        <!-- Vendor -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
        <script src="master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-business-consulting.less"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/common/common.min.js"></script>
        <script src="vendor/jquery.validation/jquery.validation.min.js"></script>
        <script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
        <script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
        <script src="vendor/isotope/jquery.isotope.min.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendor/vide/vide.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="js/theme.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Demo -->
        <script src="js/demos/demo-business-consulting.js"></script>

        <!-- Theme Custom -->
        <script src="js/custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="js/theme.init.js"></script>





        <script src="master/analytics/analytics.js"></script>

        <%-- jquery to stop user from entering letters --%>
        <script type="text/javascript">
            function fun_AllowOnlyAmountAndDot(txt) {
                if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {
                    var txtbx = document.getElementById(txt);
                    var amount = document.getElementById(txt).value;
                    var present = 0;
                    var count = 0;

                    if (amount.indexOf(".", present) || amount.indexOf(".", present + 1));
                    {
                        // alert('0');
                    }

                    /*if(amount.length==2)
                    {
                      if(event.keyCode != 46)
                      return false;
                    }*/
                    do {
                        present = amount.indexOf(".", present);
                        if (present != -1) {
                            count++;
                            present++;
                        }
                    }
                    while (present != -1);
                    if (present == -1 && amount.length == 0 && event.keyCode == 46) {
                        event.keyCode = 0;
                        //alert("Wrong position of decimal point not  allowed !!");
                        return false;
                    }

                    if (count >= 1 && event.keyCode == 46) {

                        event.keyCode = 0;
                        //alert("Only one decimal point is allowed !!");
                        return false;
                    }


                    return true;
                }
                else {
                    event.keyCode = 0;
                    //alert("Only Numbers with dot allowed !!");
                    return false;
                }

            }

        </script>
        <%-- put comma when typing naira --%>
        <script type="text/javascript">

            function Comma(Num) {
                Num += '';
                Num = Num.replace(/,/g, '');

                x = Num.split('.');
                x1 = x[0];

                x2 = x.length > 1 ? '.' + x[1] : '';


                var rgx = /(\d)((\d{3}?)+)$/;

                while (rgx.test(x1))

                    x1 = x1.replace(rgx, '$1' + ',' + '$2');

                return x1 + x2;

            }
        </script>


    </form>
</body>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-business-consulting.less"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Demo -->
<script src="js/demos/demo-business-consulting.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>
</html>
