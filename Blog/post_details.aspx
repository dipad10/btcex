﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Blog/blog.master" AutoEventWireup="false" CodeFile="post_details.aspx.vb" Inherits="Blog_post_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">
					<div class="row pt-xs pb-xl mb-md">
						<div class="col-md-12">
                            <asp:DataList ID="DataList1" DataSourceID="SqlDataSource1" runat="server">
                                <ItemTemplate>
                                    <div class="row mt-sm mb-sm">
								<article class="blog-post">
									<div class="col-md-12">
										<div class="post-infos mb-xl">
											<span class="info posted-by">
												Posted by:
												<span class="post-author font-weight-semibold text-color-dark">
													<%# Eval("bywho") %>
												</span>
											</span>
										
											<span class="info like ml-xlg">
												Post Date:
												<span class="like-number font-weight-semibold custom-color-dark">
													<%# Eval("date") %>
												</span>
											</span>
										</div>

										<hr class="solid">

										<p class="lead mb-xl"><%#Eval("sdesc") %></p>

										<img src="<%#Eval("image") %>" style="max-width: 350px;" class="img-responsive pull-right ml-xl mb-xl" alt="">

										
										<p>
                                            <%#Eval("fdesc")%>
										</p>

										
										

									</div>
								</article>
							</div>
                                </ItemTemplate>
                            </asp:DataList>
							
                            	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:nairaConnectionString %>" SelectCommand="SELECT * FROM [blog] WHERE ([id] = @id)">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="id" QueryStringField="id" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
						</div>
					</div>
				</div>
</asp:Content>

