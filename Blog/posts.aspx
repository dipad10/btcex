﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Blog/blog.master" AutoEventWireup="false" CodeFile="posts.aspx.vb" Inherits="Blog_posts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row pt-xs pb-xl mb-md">
            <div class="col-md-12">
      <asp:Label ID="Label1" CssClass="text-capitalize text-info" Visible="false" runat="server" Text="No Blog Posts"></asp:Label>
              
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div class="row mt-sm mb-sm">
                    <article class="blog-post">
                        <div class="col-sm-8 col-md-5">
                            <div class="blog-post-image-wrapper">
                                <a href="post_details.aspx?id=<%#Eval("id") %>" title="Read More">
                                    <img src="<%#Eval("image") %>" alt="" class="img-responsive mb-lg">
                                </a>
                                <span class="blog-post-date background-color-primary text-color-light font-weight-bold">
												<span class="month-year font-weight-light"><%#Eval("date")%>
                                                </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <h2><%#Eval("title") %></h2>
                            <p><%#Eval("sdesc") %></p>
                            <hr class="solid">
                            <div class="post-infos">
                                <span class="info posted-by">Posted by:
												<span class="post-author font-weight-semibold text-color-dark"><%#Eval("bywho") %>
                                                </span>
                                </span>
                               
                            </div>
                            <a class="btn btn-borders custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase mt-xlg" href="post_details.aspx?id=<%#Eval("id") %>" title="Read More">Read More</a>
                        </div>
                    </article>
                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        
                        <asp:Button ID="Btnloadmore" OnClick="Btnloadmore_Click" CssClass="btn radius-50 btn-default btn-animated" runat="server" Text="Older posts" />

                                     <asp:UpdateProgress ID="UpdateProgress1" ClientIDMode="Static" DisplayAfter="10" runat="server">
                                <ProgressTemplate>
                                    <img src="/img/728.gif" alt="wait" width="45" height="45" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>

                    </ContentTemplate>
                </asp:UpdatePanel>
                

                <hr class="solid tall mt-xl">

            </div>
        </div>
    </div>
</asp:Content>

