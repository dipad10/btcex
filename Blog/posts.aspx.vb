﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Partial Class Blog_posts
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("nairaConnectionString").ConnectionString)
    Private num As Integer = 0


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            num = 3
            ViewState("num") = num
            BindRepeater(num)
        End If


    End Sub

    Protected Sub Btnloadmore_Click(sender As Object, e As EventArgs)
        Dim numval As Integer = Convert.ToInt32(ViewState("num")) + 3
        BindRepeater(numval)
        ViewState("num") = numval
    End Sub

    Private Sub BindRepeater(numofrows As Integer)

        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try
            Dim rCount As Integer = rowCount()
            If numofrows > rCount Then
                Btnloadmore.Visible = False
            End If
            cmd = New SqlCommand("SELECT TOP (@topVal) * FROM Blog Order by id DESC", _Connection)
            cmd.Parameters.AddWithValue("@topVal", numofrows)
            cmd.CommandType = CommandType.Text
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                Repeater1.DataSource = dt
                Repeater1.DataBind()
            Else
                Label1.Visible = True


            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
        Finally
            _Connection.Close()
            cmd.Dispose()
            adp = Nothing
            dt.Clear()
            dt.Dispose()
        End Try

    End Sub

    Protected Function rowCount() As Integer
        Dim noofrows As Integer = 0
        Dim cmd As New SqlCommand("SELECT COUNT(*) FROM Blog", _Connection)
        cmd.CommandType = CommandType.Text
        Try
            _Connection.Open()

            noofrows = Convert.ToInt32(cmd.ExecuteScalar())
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
        Finally
            _Connection.Close()
            cmd.Dispose()
        End Try
        Return noofrows
    End Function
End Class
