﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/Header.ascx" TagName="n2theader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/footer.ascx" TagName="n2tfooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html>

<head runat="server">

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>BtcEx | Nigeria's favourite Bitcoin Exchange Site</title>

    <meta name="keywords" content="Exchange bitcoin, naira, btc, ex, exchanger, nigeria, bitcoin exchange, nairaex, Exchnage, btc, Ex, btcex, change yourbitcoin, bitcoin, blockchain" />
    <meta name="description" content="Nigeria's No1. Bitcoin Exchange site">
    <meta name="author" content="BtcEX">
    <meta name="theme-color" content="#4dbb6d" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/favicon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CSintony:400,700" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-business-consulting.css">
    <script src="master/style-switcher/style.switcher.localstorage.js"></script>

    <!-- Demo CSS -->
    <link rel="stylesheet" href="css/demos/demo-business-consulting.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>


</head>
<body>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type="text/javascript">window.$crisp = []; window.CRISP_WEBSITE_ID = "a9b456e9-59ec-456b-b180-51b4cce1f8c8"; (function () { d = document; s = d.createElement("script"); s.src = "https://client.crisp.im/l.js"; s.async = 1; d.getElementsByTagName("head")[0].appendChild(s); })();</script>
    <!-- {/literal} END JIVOSITE CODE -->
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="body">
            <uc1:n2theader ID="n2theader" runat="server" />
            <div role="main" class="main">
        
                <div class="slider-container rev_slider_wrapper" style="height: 100%;">
                    <div id="revolutionSlider" class="slider rev_slider manual">
                        <ul>
                            <li data-transition="fade">
                                <img src="img/demos/business-consulting/slides/slide-2.jpg"
                                    alt=""
                                    data-bgposition="center center"
                                    data-bgfit="cover"
                                    data-bgrepeat="no-repeat"
                                    data-bgparallax="1"
                                    class="rev-slidebg">

                                <h1 class="tp-caption custom-secondary-font font-weight-bold text-color-light"
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                    data-y="center" data-voffset="['-80','-80','-80','-40']"
                                    data-start="800"
                                    data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 32px;">Nigeria's #1</h1>


                                <div class="tp-caption custom-secondary-font font-weight-bold text-color-light"
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                    data-y="center" data-voffset="['-42','-42','-42','2']"
                                    data-start="800"
                                    data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 42px;">
                                    Bitcoin Exchange Platform
                                </div>

                                <a href="/private/register" class="btn btn-primary tp-caption text-uppercase text-color-light custom-border-radius"
                                    data-hash
                                    data-hash-offset="85"
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                    data-y="center" data-voffset="['60','60','60','100']"
                                    data-start="1500"
                                    style="font-size: 12px; padding: 15px 6px;"
                                    data-transform_in="y:[-300%];opacity:0;s:500;">Get Started</a>
                            </li>
                            <li data-transition="fade">
                                <img src="img/slides/btcex original pic.png"
                                    alt=""
                                    data-bgposition="center center"
                                    data-bgfit="cover"
                                    data-bgrepeat="no-repeat"
                                    data-bgparallax="1"
                                    class="rev-slidebg">

                                <h1 class="tp-caption custom-secondary-font font-weight-bold text-color-light"
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                    data-y="center" data-voffset="['-80','-80','-80','-40']"
                                    data-start="800"
                                    data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 42px;">Get Bitcoins Instantly!</h1>

                                <div class="tp-caption custom-secondary-font font-weight-bold text-color-light"
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                    data-y="center" data-voffset="['-42','-42','-42','2']"
                                    data-start="800"
                                    data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 32px;">
                                    <small>100% safe & secure</small><br />
                                  
                                </div>

                                <a href="/private/register" class="btn btn-primary tp-caption text-uppercase text-color-light custom-border-radius"
                                    data-hash
                                    data-hash-offset="85"
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                    data-y="center" data-voffset="['60','60','60','100']"
                                    data-start="1500"
                                    style="font-size: 12px; padding: 15px 6px;"
                                    data-transform_in="y:[-300%];opacity:0;s:500;">Get Started</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <section class="looking-for custom-position-1">
                    <div class="container custom-md-border-top">
                        <div class="row">
                            <div class="col-sm-6 col-md-8">
                                <div class="looking-for-box">
                                    <h2>- <span class="text-lg custom-secondary-font">Need Any Help</span><br>
                                        Contact our Customer Care</h2>

                                </div>
                            </div>

                            <div class="col-sm-3 col-md-2">
                                <a class="text-decoration-none" href="mail:Support@BtcEx.com.ng" target="_blank" title="Email Us Now">
                                    <span class="custom-call-to-action mt-xlg">
                                        <span class="action-title text-color-primary">Email Us Now</span>
                                        <span class="action-info text-color-light">support@btcex.com.ng</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <br />
                <style>
                    #btc_video.playback {
                        border-radius: 15px;
                        -webkit-border-radius: 15px;
                        -moz-border-radius: 15px;
                    }

                    #btc_video {
                        width: 100%;
                        height: 315px;
                        position: relative;
                        border-radius: 30px;
                        -webkit-border-radius: 30px;
                        -moz-border-radius: 30px;
                        overflow: hidden;
                        -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.4);
                        -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.4);
                        box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.4);
                        /* cursor: pointer; */
                    }

                    #btc_bonus {
                        margin-top: 50px;
                        background: #fbf6e8;
                        padding: 40px 0 30px;
                    }

                    .btc-medal {
                        margin-top: -75px;
                    }

                    .calculator {
                        text-align: center;
                    }

                    .mydropdown {
                        background: #efefef;
                        color: rgba(0,0,0,0.73);
                        cursor: default;
                        display: inline-block;
                        line-height: 48px;
                        padding: 2px 0 1px 16px;
                        position: relative;
                        text-align: left;
                        text-shadow: none;
                        width: 90px;
                        z-index: 1;
                        outline: none;
                        height: 42px;
                        border: 0;
                        box-shadow: none;
                        font-weight: 700;
                        text-transform: uppercase;
                    }

                    .mytextbox {
                        background: #efefef;
                        color: rgba(0,0,0,0.73);
                        cursor: auto;
                        display: inline-block;
                        line-height: 48px;
                        padding: 2px 0 1px 16px;
                        position: relative;
                        text-align: left;
                        text-shadow: none;
                        width: 215px;
                        z-index: 1;
                        outline: none;
                        height: 42px;
                        border: 0;
                        box-shadow: none;
                        font-weight: 700;
                        text-transform: uppercase;
                    }

                    .calculator span {
                        position: relative;
                        display: inline-block;
                        margin: 30px 10px;
                    }
                </style>
                <section class="about-us custom-section-padding" id="about-us">
                    <div class="container">
                        <div class=" col-md-12 calculator">
                            <h4 class="sub-title text-center">Price Calculator</h4>
                            <p class="lead small text-center"><b>Calculate the price to buy or sell Bitcoin in Naira</b></p>
                            <asp:Label ID="lbltodaybtc" Style="display:none;" runat="server" Text=""></asp:Label>
                              <asp:Label ID="Labelbuyprice" Style="display:none;" runat="server" Text=""></asp:Label>
                              <asp:Label ID="Labelsellprice" Style="display:none;" runat="server" Text=""></asp:Label>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList OnSelectedIndexChanged="ddltype_SelectedIndexChanged" data-plugin-tooltip="" data-toggle="tooltip" data-original-title="Buy or Sell?" AutoPostBack="true" ID="ddltype" CssClass="form-control mydropdown" runat="server">
                                        <asp:ListItem Value="BUY">BUY</asp:ListItem>
                                        <asp:ListItem Value="SELL">SELL</asp:ListItem>
                                    </asp:DropDownList>

                                      <span>
                                        <i class="fa-bitcoin fa fa-2x"></i>
<%--                                        <button data-plugin-tooltip="" type="button" class="btn btn-default mr-lg pull-left" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom">Tooltip on Bottom</button>--%>
                                        <asp:TextBox ID="txtbtc" data-plugin-tooltip="" data-toggle="tooltip" data-original-title="Amount in Bitcoins" onkeypress="MultiplyByHidden()" onchange="MultiplyByHidden()" onkeyup="MultiplyByHidden()" CssClass="form-control mytextbox" Text="1" runat="server"></asp:TextBox>
                                    </span>
                                     <span>
                                        <b class="fa-2x">₦</b>
                                        <asp:TextBox ID="txtnaira" data-plugin-tooltip="" data-toggle="tooltip" data-original-title="Amount In Naira" onkeypress="MultiplyByHidden2()" onchange="MultiplyByHidden2()" onkeyup="MultiplyByHidden2()" CssClass="form-control mytextbox" Text="" runat="server"></asp:TextBox>
                                    </span>
                                  
                                   
                                  <%--  <span>
                                        <asp:Button ID="Button1" Style="margin-top: -8px;" CssClass="btn btn-borders custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" runat="server" Text="Get Price" />
                                    </span>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%-- <br />
                            <p class="text-center">
                                
                            </p>--%>
                        </div>
                    </div>
                </section>

                <style>
                    .prod-info .img-holder {
    height: 120px;
}
                </style>
                <section class="section-secondary custom-section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 center">
                                <h2 class="font-weight-bold text-color-dark mb-md">- Why BtcEx?</h2>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 prod-info text-center">
                        <div class="img-holder"><img src="/img/bitcoin2.png" width="81" height="81" alt="Bitcoin Exchange"></div>
                        <p class="lead font-size-md font-weight-bold">Bitcoin Exchange</p>
                        <p class="font-size-sm">Same Day Funding & Withdrawal into any Nigerian banks and bitcoin wallets. All fees inclusive in the rate.</p>
                    </div>
                                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 prod-info text-center">
                        <div class="img-holder"><img src="/img/mastercard.png" width="81" height="81" alt="Multiple Payment Options"></div>
                        <p class="lead font-size-md font-weight-bold">Multiple payment options</p>
                        <p class="font-size-sm">Accept payments in over 40 currencies from VISA, Mastercard, Verve, and American Express. You can also pay directly from your bank accounts.</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 prod-info text-center">
                        <div class="img-holder"><img src="/img/shield.png" width="81" height="81" alt="Secured Payments"></div>
                        <p class="lead font-size-md font-weight-bold">World-class, bank-level security</p>
                        <p class="font-size-sm">All transactions are processed via a PCIDSS compliant and 3D Secure payment gateway via an SSL encrypted channel.</p>
                    </div>
                    
                 
                    
                  
                    
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 prod-info text-center">
                        <div class="img-holder"><img src="/img/customer-service.png" width="81" height="81" alt="Support Services"></div>
                        <p class="lead font-size-md font-weight-bold">Support Services</p>
                        <p class="font-size-sm">Our Customer care Representatives are online 24/7 to Serve you and also aiming to respond as fast as possible</p>
                    </div>
      
                  </div>
                     <%--       <div class="col-md-3">
                                <a href="#" class="text-decoration-none appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                                    <div class="feature-box custom-feature-box feature-box-style-2">
                                        <div class="feature-box-icon">
                                            <img src="img/Bitcoin-icon.png" alt="">
                                        </div>
                                        <div class="feature-box-info ml-md">
                                            <h4 class="font-weight-normal text-lg">Bitcoin Exchange</h4>
                                            <p>Same Day Funding & Withdrawal into any Nigerian banks and bitcoin wallets. All fees inclusive in the rate.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="text-decoration-none appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="100">
                                    <div class="feature-box custom-feature-box feature-box-style-2">
                                        <div class="feature-box-icon">
                                            <img src="img/icons/security-icon-big.gif" alt="">
                                        </div>
                                        <div class="feature-box-info ml-md">
                                            <h4 class="font-weight-normal text-lg">Trusted and Secured</h4>
                                            <p>We undergo regular IT security and financial audits.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="text-decoration-none appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="200">
                                    <div class="feature-box custom-feature-box feature-box-style-2">
                                        <div class="feature-box-icon">
                                            <img src="img/icons/support-grey.png" alt="">
                                        </div>
                                        <div class="feature-box-info ml-md">
                                            <h4 class="font-weight-normal text-lg">Support Services</h4>
                                            <p>Our Customer care Representatives are online 24/7 to Serve you.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>--%>
                        </div>
                        

                    </div>
                </section>

                


                <section class="custom-section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <p class="lead font-weight-normal text-color-primary">Our Top buyers</p>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>

                                                <th>Names
                                                </th>
                                                <th>BTC
                                                </th>
                                                <th>Amount
                                                </th>
                                                <th>Payment Method
                                                </th>
                                                <th>TYPE
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <asp:Repeater ID="RepeaterBuy" runat="server">
                                                <ItemTemplate>

                                                    <tr>

                                                        <td><%#Eval("Username")%></td>
                                                        <td><%#Eval("Bitcoinamt")%> BTC</td>
                                                        <td><%#Eval("NAIRAamt", "₦{0:#,##0}")%></td>
                                                        <td><%#Eval("Paymentmethod")%></td>
                                                        <td><%#Eval("Type")%></td>

                                                    </tr>

                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <p class="lead font-weight-normal text-color-primary">Our Top Sellers</p>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>

                                                <th>Names
                                                </th>
                                                <th>BTC
                                                </th>
                                                <th>Amount
                                                </th>
                                                <th>Payment Method
                                                </th>
                                                <th>TYPE
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="Repeatersell" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("Username")%></td>
                                                        <td><%#Eval("Bitcoinamt")%> BTC</td>
                                                        <td><%#Eval("NAIRAamt", "₦{0:#,##0}")%></td>
                                                        <td><%#Eval("Paymentmethod")%></td>
                                                        <td><%#Eval("Type")%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>


                <hr />
                <section class="custom-section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="font-weight-bold text-color-dark">- What our customers say about us</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="owl-carousel show-nav-title custom-dots-style-1 custom-dots-position custom-xs-arrows-style-2 mb-none" data-plugin-options="{'items': 1, 'autoHeight': true, 'loop': false, 'nav': false, 'dots': true}">
                                    <div>
                                        <div class="col-xs-8 col-sm-4 col-md-2 center pt-xlg">
                                            <img src="img/clients/zimbabwe6.jpg" alt class="img-responsive custom-rounded-image" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-10">
                                            <div class="testimonial custom-testimonial-style-1 testimonial-with-quotes mb-none">
                                                <blockquote class="pb-sm">
                                                    <p>It was my first bitcoin purchase success after a very bad experience elsewhere, BtcEx is great, I highly recommend BtcEx, they have a customer for life.</p>
                                                </blockquote>
                                                <div class="testimonial-author pull-left">
                                                    <p><strong>Olumide Adesanya</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col-xs-8 col-sm-4 col-md-2 center pt-xlg">
                                            <img src="img/clients/15400567_10154152588593034_7739944654741635566_n.jpg" alt class="img-responsive custom-rounded-image" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-10">
                                            <div class="testimonial custom-testimonial-style-1 testimonial-with-quotes mb-none">
                                                <blockquote class="pb-sm">
                                                    <p>What is most impressive about Btcex is that the transaction was a non-event - I specified the amount of Bitcoin I wanted, Transferred the money to the account, and had the Bitcoins in my wallet 38 minutes later - that was my first transaction, so they also verified my ID during this time.</p>
                                                </blockquote>
                                                <div class="testimonial-author pull-left">
                                                    <p><strong>Jessica Omofuma</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col-xs-8 col-sm-4 col-md-2 center pt-xlg">
<i class="fa-4x fa fa-user-circle"></i>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-10">
                                            <div class="testimonial custom-testimonial-style-1 testimonial-with-quotes mb-none">
                                                <blockquote class="pb-sm">
                                                    <p>Super fast, and reasonable fees...very pleased! Will definitely be getting more Bitcoin through Btcex.com.ng! :-)</p>
                                                </blockquote>
                                                <div class="testimonial-author pull-left">
                                                    <p><strong>James Oka</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>








                <%-- insert footer --%>
                <section class="section section-text-light section-background m-none" style="background: url('img/demos/business-consulting/contact/contact-background.jpg'); background-size: cover;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="font-weight-bold">- Contact Us</h2>
                                <div class="col-md-6 pl-none">
                                    <h4 class="mb-xs">Call Us</h4>
                                    <a href="tel:+2348166435654" class="text-decoration-none" target="_blank" title="Call Us">
                                        <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">Phone
										
                                            <span class="info text-lg">+2348166435654
											</span>
                                        </span>
                                    </a>
                                </div>

                                <div class="col-md-6 pl-none">
                                    <h4 class="mb-xs">Mail Us</h4>
                                    <a href="mail:support@btcex.com.ng" class="text-decoration-none" target="_blank" title="Mail Us">
                                        <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">Email
										
                                            <span class="info text-lg">support@btcex.com.ng
											</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="col-md-6 pl-none custom-sm-margin-top">
                                    <h4 class="mb-xs">Social Media</h4>
                                    <ul class="social-icons custom-social-icons-style-1 custom-opacity-font">
                                        <li class="social-icons-facebook">
                                            <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-twitter">
                                            <a href="http://www.twitter.com/" target="_blank" title="Twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-instagram">
                                            <a href="http://www.instagram.com/" target="_blank" title="Instagram">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-linkedin">
                                            <a href="http://www.linkedin.com/" target="_blank" title="Linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 custom-sm-margin-top">
                                <h2 class="font-weight-bold">-Join Us Today</h2>
                                <p class="lead">Now well over 150,000 registered members in Nigeria!</p>
                                <p>
                                    <a class="btn btn-borders custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" href="/private/register">Sign Up Today</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <%-- insertfii --%>
            <uc2:n2tfooter ID="n2tfooter" runat="server" />
        </div>

        <!-- Vendor -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
        <script src="master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-business-consulting.less"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/common/common.min.js"></script>
        <script src="vendor/jquery.validation/jquery.validation.min.js"></script>
        <script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
        <script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
        <script src="vendor/isotope/jquery.isotope.min.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendor/vide/vide.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="js/theme.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Demo -->
        <script src="js/demos/demo-business-consulting.js"></script>

        <!-- Theme Custom -->
        <script src="js/custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="js/theme.init.js"></script>





        <script src="master/analytics/analytics.js"></script>
        <script type="text/javascript">
            function MultiplyByHidden() {
                switch (document.getElementById("<%=ddltype.ClientID%>").options[document.getElementById("<%=ddltype.ClientID%>").selectedIndex].text)
                {
                    case 'BUY':
                        var value1 = parseFloat(document.getElementById("<%=txtbtc.ClientID%>").value)
                        var value2 = parseFloat(document.getElementById("<%=Labelbuyprice.ClientID%>").innerText)
                        var total = value1 * value2
                        document.getElementById("<%=txtnaira.ClientID%>").value = total.toFixed(2)
                        break;
                    case 'SELL':
                        var value1 = parseFloat(document.getElementById("<%=txtbtc.ClientID%>").value)
                        var value2 = parseFloat(document.getElementById("<%=Labelsellprice.ClientID%>").innerText)
                        var total = value1 * value2
                        document.getElementById("<%=txtnaira.ClientID%>").value = total.toFixed(2)
                        break;
                }
                
    }
</script>
        <script type="text/javascript">
            function MultiplyByHidden2() {
                switch (document.getElementById("<%=ddltype.ClientID%>").options[document.getElementById("<%=ddltype.ClientID%>").selectedIndex].text) {
                    case 'BUY':
                        var value1 = parseFloat(document.getElementById("<%=txtnaira.ClientID%>").value)
                        var value2 = parseFloat(document.getElementById("<%=Labelbuyprice.ClientID%>").innerText)
                        var total = value1 / value2
                        document.getElementById("<%=txtbtc.ClientID%>").value = total.toFixed(4)
                        break;
                    case 'SELL':
                        var value1 = parseFloat(document.getElementById("<%=txtnaira.ClientID%>").value)
                        var value2 = parseFloat(document.getElementById("<%=Labelsellprice.ClientID%>").innerText)
                        var total = value1 / value2
                        document.getElementById("<%=txtbtc.ClientID%>").value = total.toFixed(4)
                        break;
                }
              
    }
</script>
        <%-- jquery to stop user from entering letters --%>
        <script type="text/javascript">
            function fun_AllowOnlyAmountAndDot(txt) {
                if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {
                    var txtbx = document.getElementById(txt);
                    var amount = document.getElementById(txt).value;
                    var present = 0;
                    var count = 0;

                    if (amount.indexOf(".", present) || amount.indexOf(".", present + 1));
                    {
                        // alert('0');
                    }

                    /*if(amount.length==2)
                    {
                      if(event.keyCode != 46)
                      return false;
                    }*/
                    do {
                        present = amount.indexOf(".", present);
                        if (present != -1) {
                            count++;
                            present++;
                        }
                    }
                    while (present != -1);
                    if (present == -1 && amount.length == 0 && event.keyCode == 46) {
                        event.keyCode = 0;
                        //alert("Wrong position of decimal point not  allowed !!");
                        return false;
                    }

                    if (count >= 1 && event.keyCode == 46) {

                        event.keyCode = 0;
                        //alert("Only one decimal point is allowed !!");
                        return false;
                    }


                    return true;
                }
                else {
                    event.keyCode = 0;
                    //alert("Only Numbers with dot allowed !!");
                    return false;
                }

            }

        </script>
        <%-- put comma when typing naira --%>
        <script type="text/javascript">

            function Comma(Num) {
                Num += '';
                Num = Num.replace(/,/g, '');

                x = Num.split('.');
                x1 = x[0];

                x2 = x.length > 1 ? '.' + x[1] : '';


                var rgx = /(\d)((\d{3}?)+)$/;

                while (rgx.test(x1))

                    x1 = x1.replace(rgx, '$1' + ',' + '$2');

                return x1 + x2;

            }
        </script>


    </form>
</body>

</html>
