﻿
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net

Partial Class _Default
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("nairaConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim query As String = Request.QueryString("msg")
        If query <> "" Then
            Session.Clear()

        End If
        If Session("uname") <> "" Then

            Dim user As naira.User = (New cls_users).SelectThisUsername(Session("uname"))


            Select Case user.permission
                Case "USER"
                    Response.Redirect("~/Private/Dashboard.aspx")
                Case "ADMIN"
                    Response.Redirect("~/Private/Admin/Dashboard.aspx")
            End Select

            ''Response.Redirect("")

        End If
        If Not Page.IsPostBack Then
            Dim todaybtc As Double = 0
            Try
                Dim uri = [String].Format("https://blockchain.info/tobtc?currency=USD&value={0}", "1")

                Dim client As New WebClient()
                client.UseDefaultCredentials = True
                Dim data = client.DownloadString(uri)

                Dim result = Convert.ToDouble(data)
                todaybtc = FormatNumber(1 / result, 2)
                Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
                Dim buyprice As Double = 0
                Dim sellprice As Double = 0
                buyprice = todaybtc * rec.BuyRate
                Labelbuyprice.Text = buyprice
                sellprice = todaybtc * rec.SellRate
                Labelsellprice.Text = sellprice
                txtnaira.Text = FormatNumber(buyprice, 2)

                bindrepeaterbuy()
                bindrepeatersell()
                lbltodaybtc.Text = todaybtc
            Catch ex As Exception
                todaybtc = 0
            End Try

           


        End If


    End Sub





    Private Sub bindrepeaterbuy()
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try

            cmd = New SqlCommand("SELECT TOP 4 * FROM Orders where NAIRAamt > 100000 and type='BUY' Order by SN DESC", _Connection)

            cmd.CommandType = CommandType.Text
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)
            RepeaterBuy.DataSource = dt
            RepeaterBuy.DataBind()

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
        Finally
            _Connection.Close()
            cmd.Dispose()
            adp = Nothing
            dt.Clear()
            dt.Dispose()
        End Try
    End Sub
    Private Sub bindrepeatersell()
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try

            cmd = New SqlCommand("SELECT TOP 4 * FROM Orders where NAIRAamt > 100000 and type='SELL' Order by SN DESC", _Connection)

            cmd.CommandType = CommandType.Text
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)
            Repeatersell.DataSource = dt
            Repeatersell.DataBind()

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
        Finally
            _Connection.Close()
            cmd.Dispose()
            adp = Nothing
            dt.Clear()
            dt.Dispose()
        End Try
    End Sub

    Protected Sub ddltype_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
        Select Case ddltype.SelectedValue
            Case "BUY"
                Dim buyprice As Double = 0
                Dim btcamount As Double = txtbtc.Text
                buyprice = CDbl(lbltodaybtc.Text) * rec.BuyRate
                txtnaira.Text = FormatNumber(btcamount * buyprice, 2)

            Case "SELL"
                Dim sellprice As Double = 0
                Dim btcamount As Double = txtbtc.Text
                sellprice = CDbl(lbltodaybtc.Text) * rec.SellRate

                txtnaira.Text = FormatNumber(btcamount * sellprice, 2)

        End Select
    End Sub


    'Protected Sub txtbtc_TextChanged(sender As Object, e As EventArgs)
    '    Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
    '    Select Case ddltype.SelectedValue
    '        Case "BUY"
    '            Dim buyprice As Double = 0
    '            Dim btcamount As Double = txtbtc.Text
    '            buyprice = CDbl(lbltodaybtc.Text) * rec.BuyRate
    '            txtnaira.Text = FormatNumber(btcamount * buyprice, 2)
    '        Case "SELL"
    '            Dim sellprice As Double = 0
    '            Dim btcamount As Double = txtbtc.Text
    '            sellprice = CDbl(lbltodaybtc.Text) * rec.SellRate

    '            txtnaira.Text = FormatNumber(btcamount * sellprice, 2)

    '    End Select

    'End Sub

    'Protected Sub txtnaira_TextChanged(sender As Object, e As EventArgs)
    '    Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
    '    Select Case ddltype.SelectedValue
    '        Case "BUY"
    '            Dim buyprice As Double = 0
    '            Dim nairamount As Double = txtnaira.Text
    '            buyprice = CDbl(lbltodaybtc.Text) * rec.BuyRate
    '            txtbtc.Text = FormatNumber(nairamount / buyprice, 8)
    '        Case "SELL"
    '            Dim sellprice As Double = 0
    '            Dim nairamount As Double = txtnaira.Text
    '            sellprice = CDbl(lbltodaybtc.Text) * rec.SellRate
    '            txtbtc.Text = FormatNumber(nairamount / sellprice, 8)

    '    End Select
    'End Sub
End Class
