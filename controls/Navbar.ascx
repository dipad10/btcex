﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Navbar.ascx.vb" Inherits="Controls_Navbar" %>
<div class="app-bar darcula" data-role="appbar">
    <a href="/private/dashboard.aspx" class="app-bar-element text-shadow text-bold branding">
        <img style="width:150px; height:40px;" alt="BtcEx" src="/img/b28d33c3-1a02-4d86-9756-1f03d979d7d6.png">
    </a>
    <div class="app-bar-element place-right">

        <span id="welcome" runat="server" class="dropdown-toggle text-secondary text-bold"><span class="mif-user"></span></span>
        <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" style="width: 220px;">

            <ul class="unstyled-list fg-dark">
                <li><a href="/private/profile/profile_edit.aspx" class="text-secondary fg-white text-bold fg-hover-yellow">Profile</a></li>
                <li><a href="/private/Allorders.aspx" class="text-secondary fg-white text-bold fg-hover-yellow">All Orders</a></li>
                <li><a href="/private/profile/bank_edit.aspx" class="text-secondary fg-white text-bold fg-hover-yellow">Setup PaymentInfo</a></li>
                <li><a href="/default.aspx?msg=off" class="text-secondary fg-white text-bold fg-hover-yellow">Logout</a></li>
            </ul>
        </div>
    </div>

    <ul class="app-bar-menu place-right small-dropdown">

         <li id="backtocorporate" runat="server" data-flexorderorigin="10" data-flexorder="2">
            <a href="/default.aspx?msg=off" class=""><span class="text-secondary text-bold">Back to Corporate Website</span></a>

        </li>
             
        <li id="modulelogin" class="place-right" runat="server" data-flexorderorigin="10" data-flexorder="11">
            <a href="/private/login.aspx" class=""><span class="text-secondary text-bold">Login</span></a>

        </li>

        <li id="moduleregister" runat="server" data-flexorderorigin="10" data-flexorder="11">
            <a href="/private/register.aspx" class=""><span class="text-secondary text-bold">Register</span></a>

        </li>
             

        <li id="modulebuy" class="place-right" runat="server" data-flexorderorigin="10" data-flexorder="11">
            <a href="/private/buy_orders.aspx" class=""><span class="text-secondary text-bold"><i class="icon mif-dollars"></i>Buy Orders</span></a>

        </li>

        <li id="Modulesell" runat="server" data-flexorderorigin="10" data-flexorder="11">
            <a href="/private/sell_orders.aspx" class=""><span class="text-secondary text-bold"><i class="icon mif-coins"></i>Sell Orders</span></a>

        </li>

        <li id="moduleadmin" runat="server" data-flexorderorigin="1" data-flexorder="2" class="">
            <a href="#" class="dropdown-toggle  text-secondary"><span class="text-secondary text-bold">[Admin]</span></a>
            <ul class="d-menu" data-role="dropdown" data-no-close="true" style="display: none;">
                   <li class="text-secondary text-bold"><a class=" fg-white" href="/Private/Admin/all_transactions.aspx">All Orders</a></li>
                <li class="text-secondary text-bold"><a class=" fg-white" href="/Private/Admin/awaiting_orders.aspx">All Pending Orders</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/Private/Admin/Completed_orders.aspx">All Completed Orders</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/Private/Admin/rates.aspx">Rates</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/Private/Admin/announcement.aspx">Public Announcement</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/Private/Admin/manageusers.aspx">Manage Users</a></li>

            </ul>
        </li>


    </ul>

    <div class="app-bar-pullbutton automatic" style="display: none;"></div>
    <div class="clearfix" style="width: 0;"></div>
    <nav class="app-bar-pullmenu hidden flexstyle-app-bar-menu" style="display: none;">
        <ul class="app-bar-pullmenubar hidden app-bar-menu">
             <li>
        </li>
        </ul>
    </nav>
</div>


