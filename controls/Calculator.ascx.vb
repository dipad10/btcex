﻿Imports System.Net

Partial Class controls_Calculator
    Inherits System.Web.UI.UserControl

    'Protected Sub btngetprice_Click(sender As Object, e As EventArgs)
    '    If txtBitcoin.Text = 0 Or txtBitcoin.Text = "" Then
    '        Response.Write("<script>alert('Naira Amount cannot be 0 or empty, Please Insert your Btc Amount and click Get Price!');</script>")
    '        Exit Sub

    '    End If

    '        Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
    '        Dim buyprice As Double = 0
    '        Dim output As Double = 0

    '    buyprice = CDbl(lbltodaybtc.Text) * rec.BuyRate
    '        output = txtBitcoin.Text * buyprice
    '        txtnaira.Text = FormatNumber(output, 2)



    'End Sub

    Public Function getbtcValue() As String
        Return Me.txtBitcoin.Text
    End Function

    Public Function getnairaValue() As String
        Return Me.txtnaira.Text
    End Function

    'Protected Sub txtnaira_TextChanged(sender As Object, e As EventArgs)
    '    Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)

    '            Dim buyprice As Double = 0
    '            Dim nairamount As Double = txtnaira.Text
    '            buyprice = CDbl(lbltodaybtc.Text) * rec.BuyRate
    '    txtBitcoin.Text = FormatNumber(nairamount / buyprice, 8)

    'End Sub

    'Protected Sub txtBitcoin_TextChanged(sender As Object, e As EventArgs)
    '    Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)

    '            Dim buyprice As Double = 0
    '    Dim btcamount As Double = txtBitcoin.Text
    '            buyprice = CDbl(lbltodaybtc.Text) * rec.BuyRate
    '            txtnaira.Text = FormatNumber(btcamount * buyprice, 2)


    'End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim todaybtc As Double = 0
            Try
                Dim uri = [String].Format("https://blockchain.info/tobtc?currency=USD&value={0}", "1")

                Dim client As New WebClient()
                client.UseDefaultCredentials = True
                Dim data = client.DownloadString(uri)

                Dim result = Convert.ToDouble(data)
                todaybtc = FormatNumber(1 / result, 2)
                Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
                Dim buyprice As Double = 0

                buyprice = todaybtc * rec.BuyRate
                Labelbuyprice.Text = buyprice
            
                txtnaira.Text = FormatNumber(buyprice, 2)

              
                lbltodaybtc.Text = todaybtc
            Catch ex As Exception
                todaybtc = 0
            End Try
        End If

    End Sub
End Class
