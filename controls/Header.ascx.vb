﻿
Partial Class Header
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
        buyrate.InnerHtml = FormatNaira(rec.BuyRate)
        sellrate.InnerHtml = FormatNaira(rec.SellRate)

    End Sub
End Class
