﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="messagebox.ascx.vb" Inherits="msgbox" %>
<link href="/Private/css/metro-responsive.css" rel="stylesheet" />
<style>
    @media screen and (max-width: 1200px) {
  html {
    font-size: 100%;
  }
}
@media screen and (max-width: 768px) {
  html {
    font-size: 100%;
  }
}
@media screen and (max-width: 640px) {
  html {
    font-size: 90%;
  }
}
@media screen and (max-width: 320px) {
  html {
    font-size: 80%;
  }
}
</style>
<asp:Panel ID="PanelError" CssClass="" runat="server" Visible="False">
     <asp:Panel ID="overlayerror" Visible="false" runat="server">
            <div class="dialog-overlay op-dark"></div>

    </asp:Panel>
    <div id="dialog1" class="padding20 dialog alert" data-close-button="true" data-type="alert" style="width: 500px; height: auto; display: block; left: 550px; top: 320.5px;">
            <span class="mif-cross"> <asp:Literal ID="Literal1" runat="server"></asp:Literal></span>
          
         <button runat="server" id="btncloseerror" onserverclick="btncloseerror_ServerClick">
        <span class="dialog-close-button"></span></button>
    </div>
    
</asp:Panel>

<asp:Panel ID="PanelHelp" CssClass="" runat="server" Visible="false">
    <asp:Panel ID="overlayhelp" Visible="false" runat="server">
            <div class="dialog-overlay op-dark"></div>

    </asp:Panel>
     <div id="dialog2" class="padding20 dialog info" data-close-button="true" data-type="info" style="width: auto; height: auto; display: block; left: 550px; top: 320.5px;">
            <span class="mif-notification"> <asp:Literal ID="Literal2" runat="server"></asp:Literal></span>
           
          <button runat="server" id="btnclosehelp" onserverclick="btnclosehelp_ServerClick"><span class="dialog-close-button"></span></button></div>
  
</asp:Panel>
<asp:Panel ID="panelsuccess" CssClass="" runat="server" Visible="false">
    <asp:Panel ID="overlaysuccess" Visible="false" runat="server">
            <div class="dialog-overlay op-dark"></div>

    </asp:Panel>
     <div id="dialog3" class="padding20 dialog success" data-close-button="true" data-type="success" style="width: auto; height: auto; display: block; left: 550px; top: 320.5px;">
            <span class="mif-thumbs-up align-center"> <asp:Literal ID="Literal3" runat="server"></asp:Literal></span> 
          
        <button runat="server" id="btnclosesuccess" onserverclick="btnclosesuccess_ServerClick"><span class="dialog-close-button"></span></button>
     </div>
  
</asp:Panel>