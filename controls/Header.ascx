﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Header.ascx.vb" Inherits="Header" %>
<header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive custom-header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
    <div class="header-body">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="/default">
                            <%--                                        <h2 style="color:white; font-family:Elephant; font-weight:bold"><span><img height="50" width="50" src="/img/bitcoin-exchange-rate.png" /></span>BtcEx</h2>--%>
                            <img alt="BtcEx" width="150" height="40" src="/img/b28d33c3-1a02-4d86-9756-1f03d979d7d6.png">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-nav header-nav-dark-dropdown">
                            <span class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                <i class="fa fa-bars"></i>
                            </span>
                            <div class="header-nav-main header-nav-main-square custom-header-nav-main-effect-1 collapse">
                                <nav>
                                    <ul class="nav nav-pills" id="mainNav">

                                        <li class="">
                                            <a href="#">BUY:
														</a>
                                        </li>
                                        <li class="active">
                                            <a href="#">
                                                <span runat="server" id="buyrate"></span>
                                            </a>
                                        </li>

                                        <li class="">
                                            <a href="#">SELL:
														</a>
                                        </li>
                                        <li class="active">
                                            <a href="#">
                                                <span runat="server" id="sellrate"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="/private/register.aspx">Sign Up
														</a>
                                        </li>

                                        <li class="">
                                            <a href="/private/login.aspx">Sign In
														</a>
                                        </li>
                                        <li class="dropdown dropdown-primary">
                                            <a class="dropdown-toggle" href="#">MENU
														<i class="fa fa-caret-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="/faq.aspx">FAQ
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/Contact.aspx">Contact Us
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/Blog/posts.aspx">Blog
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="header-nav">
                            <a class="btn-sm hidden-lg hidden-md mr-4 btn btn-success" href="https://www.btcex.com.ng/private/register"><i class="fa fa-user"></i> Sign Up</a>
                            <a class="btn-sm hidden-lg hidden-md mr-4 btn btn-success" href="https://www.btcex.com.ng/private/login"><i class="fa fa-lock"></i> Sign In</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
