﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Controls_Navbar
    Inherits System.Web.UI.UserControl
    Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("nairaConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uname") = "" Then
            moduleregister.Visible = True
            modulelogin.Visible = True
            moduleadmin.Visible = False
            Modulesell.Visible = False
            modulebuy.Visible = False
            welcome.Visible = False
        Else
            Dim userrec As naira.User = (New cls_users).SelectThisUsername(Session("uname"))

            Select Case userrec.permission
                Case "USER"
                    welcome.Visible = True
                    modulebuy.Visible = True
                    Modulesell.Visible = True
                    moduleadmin.Visible = False
                    moduleregister.Visible = False
                    modulelogin.Visible = False
                Case "ADMIN"
                    welcome.Visible = True
                    modulebuy.Visible = False
                    Modulesell.Visible = False
                    moduleadmin.Visible = True
                    moduleregister.Visible = False
                    modulelogin.Visible = False
            End Select

            If Date.Now.Hour < 12 Then
                welcome.InnerText = "Good Morning, " & Session("uname").ToString() & ""

            ElseIf Date.Now.Hour < 17 Then
                welcome.InnerText = "Good Afternoon, " & Session("uname").ToString() & ""
            Else
                welcome.InnerText = "Good Evening, " & Session("uname").ToString() & ""


            End If

        End If
        'If Session("uname") = "" Then

        '    Exit Sub

        'Else
        '    Dim userrec As GHD5.User = (New cls_users).SelectThisusername(Session(("uname")))
        '    Select Case userrec.permission
        '        Case "Admin"
        '            modulehelpdeskadmin.Visible = True
        '            modulehelpdesk.Visible = False
        '            moduleadmin.Visible = True
        '            admindash.Visible = True
        '            userdash.Visible = False
        '            moduleactivity.Visible = True
        '            moduleadmin.Visible = True
        '            modulereports.Visible = True
        '            notify.Visible = True

        '        Case Else
        '            modulehelpdeskadmin.Visible = False
        '            modulehelpdesk.Visible = False
        '            moduleadmin.Visible = False
        '            admindash.Visible = False
        '            userdash.Visible = True
        '            moduleactivity.Visible = False
        '            moduleadmin.Visible = False
        '            modulereports.Visible = False
        '            notify.Visible = False

        '    End Select

        '    If Date.Now.Hour < 12 Then
        '        welcome.InnerText = "Good Morning, " & Session("uname").ToString() & ""

        '    ElseIf Date.Now.Hour < 17 Then
        '        welcome.InnerText = "Good Afternoon, " & Session("uname").ToString() & ""
        '    Else
        '        welcome.InnerText = "Good Evening, " & Session("uname").ToString() & ""


        '    End If


        '    bindnotification()


        'End If





        '_Connection.Open()
        'Dim _cmd As SqlCommand = _Connection.CreateCommand()
        '_cmd.CommandType = CommandType.Text
        '_cmd.CommandText = "select * from SecUsers where Username=( '" + Session("uname") + "') and Password=( '" + Session("password") + "')"
        '_cmd.ExecuteNonQuery()
        'Dim dr As SqlDataReader
        'dr = _cmd.ExecuteReader()
        'If dr.Read() Then


        '    If dr("permission").ToString() = "Admin" Then



        '    ElseIf dr("permission").ToString() = "Accountant" Then


        '    End If
        'Else
        '    Response.Write("<script>alert('Invalid username or Password');</script>")
        'End If

        '_Connection.Close()
        '_Connection.Dispose()
        'dr.Close()




    End Sub

    'Private Sub bindnotification()

    '    Dim dt As New DataTable()
    '    Dim cmd As SqlCommand = Nothing
    '    Dim adp As SqlDataAdapter = Nothing
    '    Try

    '        cmd = New SqlCommand("SELECT top 4 * FROM Activity order by SubmittedOn DESC", _Connection)

    '        cmd.CommandType = CommandType.Text
    '        adp = New SqlDataAdapter(cmd)
    '        adp.Fill(dt)

    '        If dt.Rows.Count > 0 Then
    '            Repeater1.DataSource = dt
    '            Repeater1.DataBind()

    '        Else
    '            Label1.Visible = True
    '            Label1.Text = "No recent activities"

    '        End If
    '    Catch ex As Exception
    '        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)

    '    End Try

    'End Sub
End Class
