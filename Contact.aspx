﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Contact.aspx.vb" Inherits="Contact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/Header.ascx" TagName="n2theader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/footer.ascx" TagName="n2tfooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>BtcEx</title>

    <meta name="keywords" content="Exchange bitcoin, bitcoin exchange, nairaex, Exchnage, btc, Ex, btcex, change yourbitcoin, bitcoin, blockchain" />
    <meta name="description" content="Nigerias No1. Bitcoin Exchange site">
    <meta name="author" content="BtcEX">
      <meta name="theme-color" content="#4dbb6d" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/favicon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CSintony:400,700" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-business-consulting.css">
    <script src="master/style-switcher/style.switcher.localstorage.js"></script>

    <!-- Demo CSS -->
    <link rel="stylesheet" href="css/demos/demo-business-consulting.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>


</head>
<body>
    <!-- BEGIN JIVOSITE CODE {literal} -->
   <script type="text/javascript">window.$crisp = []; window.CRISP_WEBSITE_ID = "a9b456e9-59ec-456b-b180-51b4cce1f8c8"; (function () { d = document; s = d.createElement("script"); s.src = "https://client.crisp.im/l.js"; s.async = 1; d.getElementsByTagName("head")[0].appendChild(s); })();</script>
    <!-- {/literal} END JIVOSITE CODE -->
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="body">
            <uc1:n2theader ID="n2theader" runat="server" />
            <div role="main" class="main">

                <section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>- Contact <span>Send us a message or call us</span></h1>
                                <ul class="breadcrumb breadcrumb-valign-mid">
                                    <li><a href="/default.aspx">Home</a></li>
                                    <li class="active">Contact Us</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                
				<div class="container">
					<div class="row pt-xs pb-xl mb-md">
						<div class="col-md-8">

							<h2 class="font-weight-bold text-color-dark">- Send a Message</h2>
							<p>If you need any help, you can contact our 24/7 customer support service via one of the methods below</p>

							<div runat="server" visible="false" class="alert alert-success mt-lg" id="contactSuccess">
								<strong>Success!</strong> Your message has been sent to us.
							</div>

							<div runat="server" visible="false" class="alert alert-danger mt-lg" id="contactError">
								<strong>Error!</strong> There was an error sending your message.
								<span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
							</div>

							<div id="contactForm" class="custom-contact-form-style-1">
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<div class="custom-input-box">
												<i class="icon-user icons text-color-primary"></i>
                                                <asp:TextBox placeholder="Your Fullname" ID="txtname" CssClass="form-control" runat="server"></asp:TextBox>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<div class="custom-input-box">
												<i class="icon-envelope icons text-color-primary"></i>

												  <asp:TextBox placeholder="Your Email Address" ID="txtemail" CssClass="form-control" runat="server"></asp:TextBox>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<div class="custom-input-box">
												<i class="icon-bubble icons text-color-primary"></i>
                                                  <asp:TextBox placeholder="Your Message" ID="txtmessage" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <asp:Button ID="btnsend" CssClass="btn btn-borders custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase mb-xlg" OnClick="btnsend_Click" runat="server" Text="Send Message" />
							
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">

							<div class="row mb-xl">
								<div class="col-md-12">
									<div class="feature-box feature-box-style-2">
										<div class="feature-box-icon mt-xs">
											<i class="icon-location-pin icons"></i>
										</div>
										<div class="feature-box-info">
											<h2 class="font-weight-bold text-color-dark">- Address</h2>
											<p class="font-size-lg"> Btcex-<br />
												Plot 84, Lekki Phase 1, Admiralty Way, Eti-Osa, Lagos.<br />
												
										</div>
									</div>
								</div>
							</div>
							<div class="row mb-xl">
								<div class="col-md-12">
									<div class="feature-box feature-box-style-2">
										<div class="feature-box-icon mt-xs">
											<i class="icon-phone icons"></i>
										</div>
										<div class="feature-box-info">
											<h2 class="font-weight-bold text-color-dark">- Phone</h2>
											<p class="font-size-lg">
												+2348012345890, +2347056723189. <br>
												
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row mb-xl">
								<div class="col-md-12">
									<div class="feature-box feature-box-style-2">
										<div class="feature-box-icon mt-xs">
											<i class="icon-envelope icons"></i>
										</div>
										<div class="feature-box-info">
											<h2 class="font-weight-bold text-color-dark">- Email</h2>
											<p class="font-size-lg">
												<a href="mailto:info@btcex.com.ng" class="text-decoration-none">Support@btcex.com.ng</a><br>
												
											</p>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>






                <%-- insert footer --%>
                <section class="section section-text-light section-background m-none" style="background: url('img/demos/business-consulting/contact/contact-background.jpg'); background-size: cover;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="font-weight-bold">- Contact Us</h2>
                                <div class="col-md-6 pl-none">
                                    <h4 class="mb-xs">Call Us</h4>
                                    <a href="tel:+2348166435654" class="text-decoration-none" target="_blank" title="Call Us">
                                        <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">Phone
										
                                            <span class="info text-lg">+2348166435654
											</span>
                                        </span>
                                    </a>
                                </div>

                                <div class="col-md-6 pl-none">
                                    <h4 class="mb-xs">Mail Us</h4>
                                    <a href="mail:support@btcex.com.ng" class="text-decoration-none" target="_blank" title="Mail Us">
                                        <span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">Email
										
                                            <span class="info text-lg">Support@btcex.com.ng
											</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="col-md-6 pl-none custom-sm-margin-top">
                                    <h4 class="mb-xs">Social Media</h4>
                                    <ul class="social-icons custom-social-icons-style-1 custom-opacity-font">
                                        <li class="social-icons-facebook">
                                            <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-twitter">
                                            <a href="http://www.twitter.com/" target="_blank" title="Twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-instagram">
                                            <a href="http://www.instagram.com/" target="_blank" title="Instagram">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li class="social-icons-linkedin">
                                            <a href="http://www.linkedin.com/" target="_blank" title="Linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 custom-sm-margin-top">
                                <h2 class="font-weight-bold">-Join Us Today</h2>
                                <p class="lead">Now well over 150,000 registered members Worldwide!</p>
                                <p>
                                    <a class="btn btn-borders custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" href="/private/register">Sign Up Today</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <%-- insertfii --%>
            <uc2:n2tfooter ID="n2tfooter" runat="server" />
        </div>

        <!-- Vendor -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
        <script src="master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-business-consulting.less"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/common/common.min.js"></script>
        <script src="vendor/jquery.validation/jquery.validation.min.js"></script>
        <script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
        <script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
        <script src="vendor/isotope/jquery.isotope.min.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendor/vide/vide.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="js/theme.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Demo -->
        <script src="js/demos/demo-business-consulting.js"></script>

        <!-- Theme Custom -->
        <script src="js/custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="js/theme.init.js"></script>





        <script src="master/analytics/analytics.js"></script>

        <%-- jquery to stop user from entering letters --%>
        <script type="text/javascript">
            function fun_AllowOnlyAmountAndDot(txt) {
                if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {
                    var txtbx = document.getElementById(txt);
                    var amount = document.getElementById(txt).value;
                    var present = 0;
                    var count = 0;

                    if (amount.indexOf(".", present) || amount.indexOf(".", present + 1));
                    {
                        // alert('0');
                    }

                    /*if(amount.length==2)
                    {
                      if(event.keyCode != 46)
                      return false;
                    }*/
                    do {
                        present = amount.indexOf(".", present);
                        if (present != -1) {
                            count++;
                            present++;
                        }
                    }
                    while (present != -1);
                    if (present == -1 && amount.length == 0 && event.keyCode == 46) {
                        event.keyCode = 0;
                        //alert("Wrong position of decimal point not  allowed !!");
                        return false;
                    }

                    if (count >= 1 && event.keyCode == 46) {

                        event.keyCode = 0;
                        //alert("Only one decimal point is allowed !!");
                        return false;
                    }


                    return true;
                }
                else {
                    event.keyCode = 0;
                    //alert("Only Numbers with dot allowed !!");
                    return false;
                }

            }

        </script>
        <%-- put comma when typing naira --%>
        <script type="text/javascript">

            function Comma(Num) {
                Num += '';
                Num = Num.replace(/,/g, '');

                x = Num.split('.');
                x1 = x[0];

                x2 = x.length > 1 ? '.' + x[1] : '';


                var rgx = /(\d)((\d{3}?)+)$/;

                while (rgx.test(x1))

                    x1 = x1.replace(rgx, '$1' + ',' + '$2');

                return x1 + x2;

            }
        </script>


    </form>
</body>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-business-consulting.less"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Demo -->
<script src="js/demos/demo-business-consulting.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>
</html>