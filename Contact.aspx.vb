﻿
Partial Class Contact
    Inherits System.Web.UI.Page

    Protected Sub btnsend_Click(sender As Object, e As EventArgs)
        Try
            Dim use As naira.User = (New cls_users).SelectThisUsername("Administrator")
            mod_main.SendHtmlFormattedEmail(use.Email, "", "Message From btcex.com.ng " & txtname.Text & "", txtmessage.Text & " Sender: " & txtemail.Text)
            contactSuccess.Visible = True

        Catch ex As Exception
            contactError.Visible = True
            contactError.InnerHtml = ex.Message
        End Try
      

    End Sub
End Class
