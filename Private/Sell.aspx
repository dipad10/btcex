﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/MasterPage.master" AutoEventWireup="false" CodeFile="Sell.aspx.vb" Inherits="Private_Sell" %>
<%@ Register Src="~/Controls/announcement.ascx" TagName="announce" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/calculatorsell.ascx" TagName="calc" TagPrefix="uc2" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <style>
        .login-form {
            width: 25rem;
            height: 18.75rem;
            position: fixed;
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
    </style>
    <link href="/private/css/index.css" rel="stylesheet" media="screen" />
    <div>
        <uc1:announce ID="announce1" runat="server" />

                <h2 runat="server" id="all" class="title align-center margin20">Sell Bitcoin</h2>
        <p class="align-center text-secondary text-bold" runat="server" id="lblrate"></p>

        <div>
            <div class="grid">
                <div class="row cells3">
                    <div class="cell">
                    </div>
                    <div class="cell">
                        <div class="padding20 bg-grayLighter block-shadow" style="opacity: 1; transform: scale(1); transition: 0.5s; width: 25rem; height: 28rem;">

                            <div class="popover marker-on-bottom bg-darkOrange">
                                <h5 class="fg-white align-center text-bold">Please Enter your Bitcoin Amount and click GetPrice</h5>

                            </div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <uc2:calc ID="calc" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="input-control success text full-size" data-role="input">
                                <label class="text-bold fg-dark uppercase" for="user_login">
                                   
                                   YOUR BANK ACCOUNT:</label><span><i class="icon mif-info place-right"></i></span>
                                 <span data-role="hint"
        data-hint-background="bg-red"
        data-hint-color="fg-white"
        data-hint-mode="2"
        data-hint="Your Preferred Bank Account">  <asp:TextBox ReadOnly="true" CssClass="text-bold block-shadow" ID="txtbank" runat="server"></asp:TextBox>
</span>
                                <%--   <input type="password" name="user_password" id="user_password" style="padding-right: 43px;">--%>
                                <button class="button helper-button reveal" tabindex="-1" type="button"><span class="mif-looks"></span></button>
                            </div>
                            <br />
                            <br />
                            <br />
                            <div class="form-actions">
                                <asp:Button ID="btnsubmit" Visible="true" Height="40px" OnClick="btnsubmit_Click" CssClass="button bg-orange text-secondary fg-white full-size rounded" runat="server" Text="Submit Order" Font-Bold="True" />

                            </div>


                        </div>
                    </div>
                    <div class="cell">
                    </div>
                </div>
            </div>
        </div>




    </div>
</asp:Content>