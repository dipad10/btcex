﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/MasterPage.master" AutoEventWireup="false" CodeFile="Payment.aspx.vb" Inherits="Private_Payment" %>

<%@ Register Src="~/Controls/announcement.ascx" TagName="announce" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <style>
        .login-form {
            width: 26rem;
            height: 19.75rem;
            position: fixed;
            background-color: #e8f1f4;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
    </style>
    <uc1:announce ID="announce1" runat="server" />


    <asp:Panel ID="panelpreview" runat="server">
        <div class="grid">
            <div class="row cells3">
                <div class="cell">
                </div>
                <div class="cell">
                    <div class="padding20 block-shadow">

                        <div class="popover marker-on-bottom bg-green">

                            <h5 class="align-center text-bold fg-white">Select Payment Mode</h5>
                        </div>
                        <br>
                        <p class="text-secondary text-bold">Bank</p>
                        <div class="input-control text">
                            <asp:TextBox ID="txtbank" Width="350px" CssClass="" runat="server"></asp:TextBox>
                        </div>
                        <%--  <span runat="server" id="txtbank" class="text-secondary fg-darkGreen"></span>--%>

                        <p class="text-secondary text-bold">Accountname</p>
                        <div class="input-control text">
                            <asp:TextBox ID="txtacctname" Width="350px" CssClass="" runat="server"></asp:TextBox>
                        </div>


                        <p class="text-secondary text-bold">Accountnumber</p>
                        <div class="input-control text">
                            <asp:TextBox ID="txtacctnum" Width="350px" CssClass="" runat="server"></asp:TextBox>
                        </div>



                        <p class="text-secondary text-bold">Amount</p>
                        <div class="input-control text">
                            <asp:TextBox ID="txtamount" Width="350px" CssClass="" runat="server"></asp:TextBox>
                        </div>


                        <p class="text-secondary text-bold">Reference ID</p>
                        <div class="input-control text">
                            <asp:TextBox ID="txtguid" Width="350px" CssClass="" runat="server"></asp:TextBox>
                        </div>

                        <p class="text-secondary text-bold">Select Mode of Payment</p>
                        <div>
                            <span data-role="hint"
                                data-hint-background="bg-red"
                                data-hint-color="fg-white"
                                data-hint-mode="2"
                                data-hint="When using creditcard remember to copy exactly the amount shown above without commas(,) and paste on the payment request page after clicking the button below">
                                <asp:RadioButton ID="radiocredit" Checked="true" AutoPostBack="true" Text="Creditcard" OnCheckedChanged="radiocredit_CheckedChanged" TextAlign="Right" CssClass="text-bold fg-gray text-small" GroupName="same" runat="server" />
                                <img src="images/credit-card-icons.png" width="200" height="30" /></span>
                        </div>

                        <div>
                            <p>
                                <span data-role="hint"
                                    data-hint-background="bg-red"
                                    data-hint-color="fg-white"
                                    data-hint-mode="2"
                                    data-hint="if you choose bank transfer, you must confirm you made the payment above by clicking the button below and upload evidence of payment(receipt, teller, bank statement etc).">
                                    <asp:RadioButton ID="radiobank" AutoPostBack="true" OnCheckedChanged="radiobank_CheckedChanged" Text="Bank Transfer/Bank Payment" TextAlign="Right" CssClass="text-bold fg-gray text-small" GroupName="same" runat="server" />
                                </span>
                            </p>
                        </div>

                        <div class="form-actions">
                           <%-- <div width="120px">--%>
                                <form method="post" action="https://voguepay.com/pay/">
                                    
                                    <input type="text" name="total" style="width: 120px" /><br />
                                    <input type="hidden" name="v_merchant_id" value="5501-0031822" />
                                    <input type="hidden" name="success_url" value="https://btcex.com.ng/private/payment_successful.aspx" />
                                    <input type="hidden" name="fail_url" value="https://btcex.com.ng/private/payment_failure.aspx" />
                                    <input type="hidden" name="memo" value="BtcEx Bitcoin Purchase" />
                                    <input type="image" src="http://voguepay.com/images/buttons/make_payment_green.png" alt="PAY" />
                                </form>
                           <%-- </div>--%>
                            <a runat="server" id="btnpaywithcreditcard" class="button bg-blue text-secondary fg-white full-size rounded" href="https://amplifypay.com/mheu9wyb3u">Pay with CreditCard</a>

                            <%--                            <button Class="button bg-orange text-secondary fg-white full-size rounded" onclick="payWithAmplify();">Make payment!</button>--%>
                            <asp:Button ID="btnsubmit" Height="40px" OnClick="btnconfirmpay_Click" CssClass="button bg-darkGreen text-secondary fg-white full-size rounded" runat="server" Text="Confirm Payment" Font-Bold="True" />
                        </div>


                        <%-- <div style="display: none;" id="mheu9wyb3u" data-host="https://amplifypay.com/">
                            <script id="amplify_load_widget" src="https://amplifypay.com/assets/amplify_load_widget.js" data-page="mheu9wyb3u" data-customer-name="" data-customer-email=""></script>
                            <script>window.addEventListener('message', function (e) { var n = e.data[0]; var ht = e.data[1]; switch (n) { case 'setHeight': f.height = ht; break; } }, false); </script>
                        </div>--%>
                    </div>
                </div>
                <div class="cell">
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="Panelconfirm" Visible="false" runat="server">
        <div class="grid">
            <div class="row cells3">
                <div class="cell">
                </div>
                <div class="cell">
                    <div class="padding20 block-shadow">

                        <div class="popover marker-on-bottom bg-darkOrange">

                            <h5 class="text-light align-center fg-white"><i class="mif-thumbs-up"></i>Acknowledge Payment</h5>
                        </div>
                        <br>
                        <p class="text-secondary text-bold">Transaction Type</p>
                        <span class="text-secondary">
                            <asp:DropDownList ID="ddltranstype" CssClass="text-secondary full-size text-bold" Height="30px" runat="server">
                                <asp:ListItem Value="BANK PAYMENT">BANK PAYMENT</asp:ListItem>
                                <asp:ListItem Value="BANK TRANSFER"></asp:ListItem>
                            </asp:DropDownList></span>
                        <hr class="thin" />
                        <p class="text-secondary text-bold">Description</p>
                        <span class="text-secondary">
                            <asp:TextBox placeholder="Tell us Anything concerning the payment" ID="txtdesc" CssClass="text-small full-size text-bold" Height="30px" runat="server"></asp:TextBox></span>
                        <hr class="thin" />
                        <p class="text-secondary text-bold">UPLOAD screenshot</p>

                        <span runat="server" id="uploadspan">
                            <dx:ASPxUploadControl ID="ASPxUploadControl1" NullText="Upload file" CssClass="shadow" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server" UploadMode="Standard">
                                <ValidationSettings AllowedFileExtensions=".txt,.jpg,.jpe,.jpeg,.doc,.png" MaxFileSize="1000000">
                                </ValidationSettings>
                            </dx:ASPxUploadControl>
                        </span>

                        <hr class="thin" />
                        <span class="text-secondary">
                            <dx:ASPxLabel ID="AllowedFileExtensionsLabel" runat="server" Text="Allowed file extensions: .jpg, .jpeg, .gif, .png." Font-Size="8pt">
                            </dx:ASPxLabel>
                            <br />
                            <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt">
                            </dx:ASPxLabel>
                        </span>


                        <div class="form-actions">
                            <asp:Button ID="btncomplete" Visible="true" Height="40px" OnClick="btncomplete_Click" CssClass="button align-center full-size bg-orange text-secondary fg-white rounded" runat="server" Text="Mark Payment Complete" Font-Bold="True" />
                        </div>


                    </div>
                </div>
                <div class="cell">
                </div>
            </div>

        </div>

        <%--<div class="panel margin10" data-role="panel">
        <div class="heading bg-darkGreen fg-white">
            <span class="icon mif-thumbs-up"></span>
            <span class="title">Acknowledge Payment</span>
        </div>
        <div class="content">
            <div class="margin20">
            <table>
                <tr>
                    <td>
                        <p class="text-secondary text-bold">Transaction Type</p>
                    </td>
                    <td>
                       
                            <asp:DropDownList ID="ddltranstype" CssClass="text-secondary text-bold" Height="30px" runat="server">
                                <asp:ListItem>BANK PAYMENT</asp:ListItem>
                                 <asp:ListItem>BANK TRANSFER</asp:ListItem>
                            </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="text-secondary text-bold">Description</p>
                    </td>
                    <td>
                       
                            <asp:TextBox placeholder="Tell us Anything concerning the payment" ID="txtdesc" CssClass="text-small text-bold" Height="30px" runat="server"></asp:TextBox>
                  </td>
                </tr>
                 <tr>
                    <td>
                        <p class="text-secondary text-bold">UPLOAD screenshot </p>
                    </td>
                    <td>
                       
                            

                    </td>
                </tr>
                 <tr>
                
                 <td>
                     <td></td>
                      <p class="text-small">
            <dx:ASPxLabel ID="AllowedFileExtensionsLabel" runat="server" Text="Allowed file extensions: .jpg, .jpeg, .gif, .png." Font-Size="8pt">
            </dx:ASPxLabel>
            <br />
            <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt">
            </dx:ASPxLabel>
        </p>
                 </td>
             </tr>
            </table>
                <br />
                 <asp:Button ID="btncomplete" Visible="true" Height="40px" OnClick="btncomplete_Click" CssClass="button bg-orange text-secondary fg-white rounded" runat="server" Text="Mark Payment Complete" Font-Bold="True" />

        </div>
    </div>
            </div>--%>
    </asp:Panel>
    <asp:Panel ID="panelsell" runat="server">
        <div class="grid">
            <div class="row cells3">
                <div class="cell">
                </div>
                <div class="cell">
                    <div class="padding20 block-shadow">

                        <div class="popover marker-on-bottom bg-darkOrange">

                            <h5 class="text-light align-center fg-white">After Sending Bitcoin, Confirm Complete Payment</h5>
                        </div>
                        <br>
                        <p class="text-secondary text-bold">Ref ID</p>
                        <span runat="server" id="txtsellrefid" class="text-secondary fg-darkGreen"></span>
                        <hr class="thin" />


                        <p class="text-secondary text-bold">Bitcoin Amount To be Sent</p>
                        <span runat="server" id="txtsellbtcamt" class="text-secondary fg-darkGreen"></span>
                        <hr class="thin" />
                        <p class="text-secondary text-bold">Naira Amount</p>
                        <span runat="server" id="txtsellnairaamt" class="text-secondary fg-darkGreen"></span>
                        <hr class="thin" />

                        <br />

                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div style="font-size: 16px; margin: 0 auto; width: 100%;" class="blockchain-btn" data-address="<%#Eval("Walletaddress")%>" data-shared="false">
                                    <div class="blockchain align-center stage-begin">
                                        <a href="#" class="button bg-darkGreen rounded fg-white text-secondary text-bold mini-button">
                                            <i class=" mif-coins"></i>View Address and Send Bitcoin
                                        </a>
                                        <%-- <img src="https://blockchain.info/Resources/buttons/donate_64.png" />--%>
                                    </div>
                                    <div class="blockchain stage-loading" style="text-align: center">
                                        <img src="https://blockchain.info/Resources/loading-large.gif" />
                                    </div>
                                    <div class="blockchain stage-ready">
                                        <p align="center text-small text-bold">Please send Exact amount of bitcoin stated above To Bitcoin Address: <b>[[address]]</b></p>
                                        <p align="center" class="qr-code"></p>
                                    </div>
                                    <div class="blockchain stage-paid text-small text-bold">Bitcoin Payment of <b>[[value]] BTC</b> Received. Thank You. Now mark payment as complete to Get your bank payment </div>
                                    <div class="blockchain stage-error text-small text-bold"><font color="red">[[error]]</font></div>
                                    <div class="form-actions">
                                        <asp:Button ID="btnmarksell" Visible="true" Height="40px" OnClick="btnmarksell_Click" CssClass="button bg-orange text-secondary fg-white full-size rounded" runat="server" Text="Mark Payment Complete" Font-Bold="True" />

                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>


                        <hr class="thin" />
                        <span class="text-secondary">
                            <span class="dxeBase" id="body_AllowedFileExtensionsLabel" style="font-size: 8pt;"><b>Note: </b>After sending the bitcoin to the given wallet address, you must mark payment complete below by clicking the button below to Complete Transactions.</span>

                        </span>
                        <br />
                        <br />



                    </div>
                </div>
                <div class="cell">
                </div>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            $("#panel").panel();
        });
    </script>

    <%-- <script src="https://amplifypay.com/assets/amplify_inline_widget.min.js"></script>

    <script>
        function payWithAmplify(e) {
            e.preventDefault();
            var inlineCollector = new AmplifyInline("NJEUEGZ22UI5MI7MZ4FQA", "46f46a54-6865-4073-9bd8-e9d68fee0c3e");
            inlineCollector.setUp({
                Amount: 25000,
                paymentDescription: "BtcEx Bitcoin Payment",
                customerEmail: "tester@domain.com",
                customerName: "Mr Tester",
                redirectUrl: "http://testdomain.com/response",
                transID: 1234567,
                planId: 302,
                onSuccess: function (response) {
                    alert(response.transactionRef);
                }
            }).showFrame();
        }
</script>--%>
</asp:Content>

