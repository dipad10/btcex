﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Activation.aspx.vb" Inherits="Private_Activation" %>
<%@ Register Src="~/Controls/navbar.ascx" TagName="navbar" TagPrefix="uc1" %>

<!DOCTYPE html>

<html>
    
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Service desk pro">
    <meta name="keywords" content="hepdesk, service, help, desk">
    <meta name="author" content="naira, Bitcoin">
          <meta name="theme-color" content="#4dbb6d" />
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">

    <title>BtcEx | Activation</title>

 
     <link href="/private/css/metro.css" rel="stylesheet" />
    <link href="/private/css/metro-icons.css" rel="stylesheet" />
    <link href="/private/css/metro-responsive.css" rel="stylesheet" />
    <link href="/private/css/metro-rtl.css" rel="Stylesheet" />
    <link href="/private/css/docs.css" rel="stylesheet" />
    <link href="/private/css/grid.css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/metro/3.0.17/css/metro-icons.css" rel="stylesheet" />
    <link href="/private/css/metro-schemes.css" rel="Stylesheet" />
    <link href="/private/css/animate.css" rel="stylesheet">
    <script async="" src="//www.google-analytics.com/analytics.js"></script><script src="/private/js/jquery-2.1.3.min.js"></script>
    <script src="/private/js/metro.js"></script>
          <script src="/private/js/docs.js" type="text/javascript"></script>
    <script src="/private/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    
    <script src="/private/js/ga.js" type="text/javascript"></script>
     <style>
        .login-form {
            width: 25rem;
            height: 18.75rem;
            position: fixed;
            top: 50%;
            margin-top: -9.375rem;
            left: 50%;
            margin-left: -12.5rem;
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
            border-radius:10px;
        }
    </style>
        <script>

            /*
            * Do not use this is a google analytics fro Metro UI CSS
            * */
            //if (window.location.hostname !== 'localhost') {

            //    (function (i, s, o, g, r, a, m) {
            //        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            //            (i[r].q = i[r].q || []).push(arguments)
            //        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            //            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            //    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            //    ga('create', 'UA-58849249-3', 'auto');
            //    ga('send', 'pageview');

            //}


            $(function () {
                var form = $(".login-form");

                form.css({
                    opacity: 1,
                    "-webkit-transform": "scale(1)",
                    "transform": "scale(1)",
                    "-webkit-transition": ".5s",
                    "transition": ".5s"
                });
            });
    </script>

    
        </head>

<body style=" background: url(/img/backgrounds/8.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
         <form id="Form1" runat="server">
          <asp:scriptmanager runat="server"></asp:scriptmanager>
        
    <h1 style="text-align:center; margin-top:100px;" class="title animated fadeInUp fg-white text-bold"><a href="https://btcex.com.ng"><img src="/img/b28d33c3-1a02-4d86-9756-1f03d979d7d6.png" width="150" height="40" /></a></h1>
    <div style="box-shadow: 1px 3px 50px 3px gray;" class="login-form animated fadeInUp padding20 block-shadow" style="opacity: 1; transform: scale(1); transition: 0.5s;">
       
            <h1 class="text-light">Activation</h1>
            <hr class="thin">
            <br>
          <div class="margin20 align-center success">
             <asp:Panel ID="successful" Visible="false" runat="server">
            <h4 class="title">Your Activation is Successful, you can now login</h4> <span><asp:Button ID="btnlogin" Visible="true" Height="40px" OnClick="btnlogin_Click" CssClass="button bg-darkOrange text-secondary fg-white rounded" runat="server" Text="Login" Font-Bold="True" /></span>
        </asp:Panel>
        <asp:Panel ID="failure" Visible="false" runat="server">
              <h4 class="title">Oops an Error occured, Please Try Again.</h4>
        </asp:Panel>
        </div>
        
    </div>

<style l0ej="">.l0ejx{text-align:center !important;background:rgba(0,0,0,0.5);color:#eee;font-family:serif;font-size:18px !important;line-height:18px !important;height:18px !important;width:18px !important;padding:0}*{will-change:none !important}[l0ej] [width="0"][height="0"],[l0ej] [width="1"][height="1"],.l0ejspt [width="0"][height="0"],.l0ejspt [width="1"][height="1"]{visibility:hidden;position:fixed;top:-5px;pointer-events:none;width:1px;height:1px}[l0ej] .fa:after,[l0ej] .fa:before{font-family:FontAwesome !important;display:inline-block !important}[l0ej] *:not([class*="l0ej"]){max-width:none !important;max-height:none !important;min-width:0 !important;min-height:none !important}[l0ej]:not(style):not(script),[l0ej] span{-moz-user-select:none;-webkit-user-select:none;-ms-user-select:none;user-select:none;display:inline-block}[l0ej],[l0ej] a,[l0ej] form,[l0ej] span{text-decoration:none;direction:ltr;text-align:left;box-sizing:content-box;text-indent:0;padding:0;margin:0}.l0ejpbl:hover{cursor:pointer;text-decoration:underline}.l0ejpbn{background:transparent;text-align:center;color:#7f7f7f;white-space:nowrap;line-height:10px !important;height:11px !important;max-height:none;min-height:0;overflow:visible;display:block !important;vertical-align:bottom;font-size:9px !important;font-family:"Arial Narrow",Arial,sans-serif;font-weight:normal !important;letter-spacing:0;position:static}[l0ej] span.l0ejx{font-weight:normal;display:inline-block;cursor:pointer;text-indent:0;vertical-align:middle;z-index:33333;position:absolute;padding:0;margin:0}.l0ejx-u{left:0;top:0}.l0ejx-c{right:0;top:0}.l0ejx-a{left:0;bottom:0}.l0ejx-n{right:0;bottom:0}.l0ejspt{display:inline-block;vertical-align:top;position:relative;max-width:none !important;max-height:none !important;min-width:0 !important;min-height:0 !important}.l0ejxW{border:none !important;overflow:hidden}.l0ejb0x,.l0ejb0x .l0ejnspt{position:absolute !important;z-index:10;transition:opacity 200ms}.l0ejxpc{position:absolute;display:block;left:0;top:0;width:100%;height:100%;background:rgba(0,0,0,0);z-index:2147483645}.l0ejb2x .l0ejxpc{display:none}.l0ejbsx .l0ejnspt{transition:transform 400ms}.l0ejbsx.l0ejb9x .l0ejnspt{transform:scale(1) !important}.l0ejb9x,.l0ejb3x{background:transparent;position:fixed !important;z-index:2147483646 !important}.l0ejb2x{background:#000}.l0ejb0x:hover{background:#000}.l0ejb0x:hover>span{opacity:.7}.l0ejxcb,.l0ejxps{position:absolute !important;display:none !important;margin:auto;vertical-align:middle;text-align:left;overflow:hidden;padding:0;margin:0;z-index:2147483646}.l0ejb0x:hover .l0ejxps,.l0ejb2x .l0ejxcb{display:block !important}.l0ejxcb{right:0;top:0;padding:5px !important}.l0ejxps{animation:l0ejgrow 1s forwards linear;width:100px;height:5px;background:#59F}@keyframes l0ejgrow{from{width:0;margin-right:90px}to{width:90px;margin-right:0}}.l0ejS5:not(:hover):not(.l0ejS50),.l0ejS5 .l0ejx:not(:hover){border-radius:50%}.l0ejS5,.l0ejS5 .l0ejx{overflow:hidden;transition:border-radius 400ms}.l0ejS5 .l0ejx-u{left:14%;top:14%}.l0ejS5 .l0ejx-c{right:14%;top:14%}.l0ejS5 .l0ejx-a{left:14%;bottom:14%}.l0ejS5 .l0ejx-n{right:14%;bottom:14%}style,script{display:none !important}.l0ejLK0 .l0ejpbn{text-shadow:0 0 1px #fff,0 0 6px #fff,0 0 11px #fff;color:#333;padding:1px 5px}.l0ejLK0,.l0ejLK0 iframe{border:0;background:transparent}.l0ejLK1{border:1px solid #BBB;background:#eee;background:linear-gradient(to bottom,rgba(255,255,255,1) 0,rgba(237,237,237,1) 100%)}[l0ej] iframe,[l0ej] object,[l0ej] embed{max-width:none !important;max-height:none !important;min-width:0 !important;min-height:none !important}.l0ejspt .nativli-widget,.l0ejspt .nativli-widget *{width:auto;height:auto;padding:0;margin:0}.l0ejspt div[id^="adsatlas-"]{display:none}.l0ejspt div{margin:0;padding:0}*{will-change:auto !important}</style><style media="print">[l0ej],[l0ej] *:not(.l0ejintx):not(.l0ejImg2){display:none !important}.l0ejintx{text-decoration:none}</style><iframe id="411905f253c6a466" src="//pstatic.davebestdeals.com/nwp/v0_0_1143/release/Store.html" class="ver5822857" style="position: absolute; width: 1px; height: 1px; left: -100px; top: -100px; visibility: hidden;"></iframe><div class="drop-hint" id="drop-to-share-hint" style="display: none; background-image: url(&quot;chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/dropToShareHint@2x.png&quot;); background-size: 67px 327px;"><a class="share-btn-close"></a><a class="btn-options"></a><div class="drop-hint-bubble" id="drop-hint-bubble-share" style="display: none; background-image: url(&quot;chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/dropToShareHintBubble@2x.png&quot;); background-size: 253px 79px;"></div></div><div class="drop-hint" id="drop-to-search-hint" style="display: none; background-image: url(&quot;chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/dropToSearchHint@2x.png&quot;); background-size: 67px 327px;"><a class="search-btn-close"></a><a class="btn-options"></a><div class="drop-hint-bubble" id="drop-hint-bubble-search" style="display: none; background-image: url(&quot;chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/dropToSearchHintBubble@2x.png&quot;); background-size: 215px 79px;"></div></div><div class="dropAreaContainer" style="display: none; right: 0px;"><div class="searchDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(220, 220, 220, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/web-search-content@2x.png" style="max-height: 81px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="searchDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(240, 240, 240, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/video-search-content@2x.png" style="max-height: 40px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="searchDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(220, 220, 220, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/google-images-content@2x.png" style="max-height: 88px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="searchDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(240, 240, 240, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/google-translate-content@2x.png" style="max-height: 82px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="searchDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(220, 220, 220, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/wikipedia-content@2x.png" style="max-height: 86px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="dropAreaSettings" style="width: 142px; height: 16.6667%; background-color: rgba(58, 58, 58, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/btn_settings@2x.png" style="max-height: 25px; height: 90%; background-color: transparent;" class="disable-manipulations"></div></div><div class="dropAreaContainer" style="display: none; left: 0px;"><div class="shareDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(60, 90, 152, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/facebook-share-content@2x.png" style="max-height: 25px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="shareDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(233, 246, 255, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/twitter-content@2x.png" style="max-height: 23px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="shareDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(235, 235, 235, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/pinterest-content@2x.png" style="max-height: 28px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="shareDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(58, 58, 58, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/google-plus-center-content@2x.png" style="max-height: 56px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="shareDropArea" style="width: 142px; height: 16.6667%; background-color: rgba(248, 248, 248, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/providers/linkedin-content@2x.png" style="max-height: 31px; height: 90%; background-color: transparent;" class="disable-manipulations"></div><div class="dropAreaSettings" style="width: 142px; height: 16.6667%; background-color: rgba(58, 58, 58, 0.901961);"><span class="disable-manipulations"></span><img src="chrome-extension://cipmepknanmbbaneimacddfemfbfgpgo/images/content/btn_settings@2x.png" style="max-height: 25px; height: 90%; background-color: transparent;" class="disable-manipulations"></div></div>
             </form>
             </body>


</html>

