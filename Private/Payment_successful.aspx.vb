﻿
Partial Class Private_Payment_successful
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim b As New cls_orders
            Dim ord As naira.Order = b.SelectThisorderid(Session("orderid"))
            ord.Paymentmethod = "CREDIT CARD"
            ord.Status = "AWAITING CONFIRMATION"
            b.Update(ord)
            'send the user a mail that order has been placed
            Dim body As String = mod_main.PopulateBodyorderplaced(ord.Type, ord.TransGUID, ord.Username, ord.Bitcoinamt, FormatNumber(ord.NAIRAamt, 2), ord.Createdon)
            Call mod_main.SendHtmlFormattedEmail(ord.Useremail, "", "BtcEx: Order Notification", body)
            'send mail to administrator
            Dim use As naira.User = (New cls_users).SelectThisUsername("Administrator")
            Dim adminbody As String = mod_main.PopulateBodyorderplacedadmin(ord.Type, ord.TransGUID, ord.Orderid, ord.Username, ord.Bitcoinamt, FormatNumber(ord.NAIRAamt, 2), ord.Createdon)
            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "BtcEx: Order Notification!(Creditcard)", adminbody)
            'update pament method
            Response.Redirect("/private/Order_details.aspx?od=" & ord.TransGUID & "")
        End If
    End Sub
End Class
