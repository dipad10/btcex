﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Private_Register
    Inherits System.Web.UI.Page

    Protected Sub btnregister_Click(sender As Object, e As EventArgs)
        'do validations
        If txtpassword.Text <> txtconfirm.Text Then
            messagebox.Visible = True
            errortext.InnerHtml = "Passwords do not match"
            Exit Sub

        End If
        If txtfullname.Text = "" Then
            messagebox.Visible = True
            errortext.InnerHtml = "Please Enter your Fullname"
            Exit Sub
        End If
        If txtusername.Text = "" Then
            messagebox.Visible = True
            errortext.InnerHtml = "Please Enter your Username"
            Exit Sub
        End If
        Dim user As naira.User = (New cls_users).SelectThisUsername(txtusername.Text)
        If user.UserName Is Nothing Then

        Else
            messagebox.Visible = True
            errortext.InnerHtml = "This User already exists. Please use another Username"
            Exit Sub
          
        End If
        Dim eml As naira.User = (New cls_users).SelectThisemail(txtemail.Text)
        If eml.Email Is Nothing Then
        Else
            messagebox.Visible = True
            errortext.InnerHtml = "This Email already exists. Please use another Email"
            Exit Sub
        End If
        If mod_main.IsValidEmail(txtemail.Text) = False Then
            messagebox.Visible = True
            errortext.InnerHtml = "This is not a valid Email"
            Exit Sub
        End If
        If txtpassword.Text = "" Or txtconfirm.Text = "" Then
            messagebox.Visible = True
            errortext.InnerHtml = "Password is required"
            Exit Sub
        End If

        Dim P As New cls_users

        Dim numbertype As String = "UserID"
        Dim auto As naira.Autosetting = (New cls_autosettings).Getautonumbervalue(numbertype)
        Dim ID As String = String.Format("USER/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)

        Dim Rec As New naira.User
        Rec.Username = txtusername.Text
        Rec.Password = txtpassword.Text
        Rec.Email = txtemail.Text
        Rec.UserID = ID
        Rec.Active = 0
        Rec.permission = "USER"
        Rec.SubmittedOn = DateTime.Now.ToString
        Rec.Fullname = txtfullname.Text
        Rec.Verified = 0
        Rec.ActivationCode = Guid.NewGuid.ToString
        Dim res As ResponseInfo = P.Insert(Rec)
        If res.ErrorCode = 0 Then
            Dim url As String = "" & ConfigurationManager.AppSettings("pathtouseractivation") & "?Acode=" & Rec.ActivationCode & ""
            Dim body As String = mod_main.PopulateBodyactivation(Rec.Username, url)
            Call mod_main.SendHtmlFormattedEmail(Rec.Email, "", "Activate your Btcex Account", body)
            'messagebox2.Visible = True
            'successtext.InnerHtml = "Registration successful. Activation email has been sent."

            'Session("uname") = Rec.Username
            Session("useremail") = Rec.Email
            Response.Redirect("/private/welcome.aspx")
        Else
            messagebox.Visible = True
            errortext.InnerHtml = (res.ErrorMessage & " " & res.ExtraMessage)
        End If

    End Sub
End Class
