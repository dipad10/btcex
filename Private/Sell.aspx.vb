﻿Imports System.Net

Partial Class Private_Sell
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs)
        If calc.getnairaValue = "" Or calc.getbtcValue = 0 Or calc.getbtcValue = "" Then
            Response.Write("<script>alert('Naira Amount cannot be 0 or empty, Please Insert your Btc Amount and click Get Price');</script>")
            Exit Sub
        End If
        If calc.getnairaValue < 10000 Then
            Response.Write("<script>alert('OOps! Sorry You can only transact N10,000 and above!');</script>")
            Exit Sub

        End If
        'If txtbank.Text = "" Then
        '    Response.Write("<script>alert('Please setup your bank details before selling bitcoin!');</script>")
        '    Exit Sub
        'End If
        Dim A As New cls_orders

        Dim rec As New naira.Order
        Dim numbertype As String = "orderid"
        Dim auto As naira.Autosetting = (New cls_autosettings).Getautonumbervalue(numbertype)
        Dim ID As String = String.Format("ODR/BUY/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)

        rec.Username = (Session("uname"))
        rec.NAIRAamt = calc.getnairaValue
        rec.Bitcoinamt = calc.getbtcValue

        rec.fullamount = String.Format("{0} for {1}", rec.Bitcoinamt, rec.NAIRAamt)
        rec.Orderid = ID
        rec.TransGUID = System.Guid.NewGuid.ToString
        rec.Status = "PENDING"
        rec.Type = "SELL"
        rec.Paymentmethod = "BITCOIN WALLET"
        rec.Duecanceldate = DateTime.Now.AddHours(2)
        rec.Createdon = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")

        rec.Active = 1

        Dim use As naira.User = (New cls_users).SelectThisUsername(rec.Username)
        rec.Userid = use.UserID
        rec.Useremail = use.Email
        rec.Bank = use.Bankname
        rec.Accountname = use.Accountname
        rec.Accountnumber = use.Accountnumber
        rec.Userfullname = use.Fullname.ToUpper
        Dim res As ResponseInfo = A.Insert(rec)
        If res.ErrorCode = 0 Then
            'successful
            Session("orderid") = rec.Orderid
            Session("userid") = rec.Userid
            Session("guid") = rec.TransGUID
            Session("amount") = rec.NAIRAamt
            Session("btcamount") = rec.Bitcoinamt
            Response.Redirect("/private/payment.aspx?op=S")
        Else
            Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
            Exit Sub

        End If



    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim todaybtc As Double = 0
            Try
                Dim uri = [String].Format("https://blockchain.info/tobtc?currency=USD&value={0}", "1")

                Dim client As New WebClient()
                client.UseDefaultCredentials = True
                Dim data = client.DownloadString(uri)

                Dim result = Convert.ToDouble(data)
                todaybtc = FormatNumber(1 / result, 2)
                Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
                Dim sellprice As Double = 0


                sellprice = todaybtc * rec.SellRate
                lblrate.InnerHtml = String.Format("RATE : 1BTC = {0}{1}", "₦", FormatNumber(sellprice, 2))
            Catch ex As Exception
                todaybtc = 0
            End Try

        

            Dim user As naira.User = (New cls_users).SelectThisUsername(Session("uname"))
            If user.Bankname Is Nothing Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('You need to setup your bank details to place a 'SELL' Order');window.location='profile/bank_edit.aspx';", True)
            Else
                txtbank.Text = String.Format("{0} {1}", user.Bankname.ToUpper, user.Accountnumber.ToUpper)
            End If

        End If
    End Sub
End Class
