﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/MasterPage.master" AutoEventWireup="false" CodeFile="order_details.aspx.vb" Inherits="Private_order_details" %>

<%@ Register Src="~/Controls/calculator.ascx" TagName="calc" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/announcement.ascx" TagName="announce" TagPrefix="uc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <link href="/private/css/index.css" rel="stylesheet" media="screen" />
    <div>
        <uc1:announce ID="announce1" runat="server" />



        <div>
            <div class="grid">
                <div class="row cells5">
           
                    <div class="cell">
                    </div>
                    <div class="cell colspan3">
                        <div class="panel success">
                            <div class="heading">
                                <span class="title">Order Information</span>
                            </div>
                            <div class="content padding10">
                                <p class="text-small text-bold" runat="server" id="message"></p>
                               
                                <hr class="thin" />
                                <table style="line-height:20px; width:100%;">
                                    <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Reference ID</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="txtrefid" class="text-secondary"></span>
                                          </td>
                                          <td></td>
                                          <td></td>
                                    </tr>
                                     <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Bitcoin Amount</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="txtbtc" class="text-secondary"></span>
                                          </td>
                                          <td></td>
                                          <td></td>
                                    </tr>
                                     <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Naira Amount</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="txtnaira" class="text-secondary"></span>
                                          </td>
                                          <td></td>
                                          <td></td>
                                    </tr>

                                     <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Destination</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="txtdestination" class="text-secondary"></span>
                                          </td>
                                          <td></td>
                                          <td></td>
                                    </tr>

                                     <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Transaction Type</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="lblbuy" class="text-secondary"></span>
                                          </td>
                                          <td></td>
                                          <td></td>
                                    </tr>
                                     <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Status</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="txtstatus" class="text-secondary text-bold fg-darkGreen"></span>
                                          </td>
                                          <td>

                                          </td>
                                          <td></td>
                                    </tr>
                                     <tr class="block-shadow">
                                        <td>
                                            <p class="text-secondary text-bold">Created</p>
                                        </td>
                                          <td>
                                              <span runat="server" id="txtcreatedon" class="text-secondary">2017-06-04</span>
                                          </td>
                                          <td>

                                          </td>
                                          <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                      <div class="cell">
                    </div>
                </div>
            </div>
        </div>




    </div>
</asp:Content>

