﻿
Partial Class Private_Activation
    Inherits System.Web.UI.Page

    Protected Sub btnlogin_Click(sender As Object, e As EventArgs)
        Response.Redirect("/private/login.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim code As String = Request.QueryString("Acode")
            If code <> "" Then
                'if activation code is not empty update active to 1
                Dim A As New cls_users
                Dim rec As naira.User = A.SelectThisguid(code)

                Try
                    'check if the user is not actvated before so it wont keep send mails over and over again wen d idiot refresh d page
                    If rec.Active = 0 Then
                        Dim body As String = mod_main.PopulateBody(rec.Username, rec.Password, rec.Email)
                        Call mod_main.SendHtmlFormattedEmail(rec.Email, "", "Welcome to BtcEx", body)
                        rec.Active = 1
                        A.Update(rec)

                        successful.Visible = True
                        failure.Visible = False
                    Else
                        'tell the user ur account has been activated already. dont send any mail again
                        successful.Visible = True
                        failure.Visible = False
                    End If

                  
                Catch ex As Exception
                    Response.Write("<script>alert('" & ex.Message & "');</script>")
                    Exit Sub
                End Try

            Else
                failure.Visible = True
                successful.Visible = False

                Exit Sub

            End If
        End If

      
    End Sub
End Class
