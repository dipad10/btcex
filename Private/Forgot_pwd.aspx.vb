﻿
Partial Class Private_Forgot_pwd
    Inherits System.Web.UI.Page

    Protected Sub btnsignin_Click(sender As Object, e As EventArgs)
        If txtemail.Text = "" Then
            messagebox.Visible = True
            messagebox.InnerHtml = "Email is Required!"
            Exit Sub

        End If

        Dim rec As List(Of naira.User) = (New cls_users).SelectThisemailist(txtemail.Text)
        If rec.Count = 0 Then
            'show error
            messagebox.Visible = True
            messagebox.InnerHtml = "Oops, This email address does not match our records."
            Exit Sub
        Else
            Dim use As naira.User = (New cls_users).SelectThisemail(txtemail.Text)
            'send password to email
            Dim body As String = mod_main.PopulateBodyforgotpwd(use.Username, use.Password)

            mod_main.SendHtmlFormattedEmail(use.Email, "", "Your Btcex Account Password", body)
            messagebox2.Visible = True
            successtext.InnerHtml = "Password has been sent to your email address."
        End If
    End Sub
End Class
