﻿
Partial Class Private_order_details
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim guid As String = Request.QueryString("od")
            Dim rec As naira.Order = (New cls_orders).SelectThistransguid(guid)
            Select Case rec.Status
                Case "PENDING"
                    If rec.Type = "BUY" Then
                        Session("orderid") = rec.Orderid
                        Session("userid") = rec.Userid
                        Session("guid") = rec.TransGUID
                        Session("amount") = rec.NAIRAamt
                        Response.Redirect("/private/payment.aspx?op=B")
                    Else
                        Session("orderid") = rec.Orderid
                        Session("userid") = rec.Userid
                        Session("guid") = rec.TransGUID
                        Session("amount") = rec.NAIRAamt
                        Session("btcamount") = rec.Bitcoinamt
                        Response.Redirect("/private/payment.aspx?op=S")

                    End If

                Case "AWAITING CONFIRMATION"
                    If rec.Type = "BUY" Then
                        message.InnerHtml = "Thank you for your Order. Once we Verify your payment, we'll transfer the Bitcoin to your designated address below. If you have questions Regarding this transactions, please send a mail to support@btcex.com.ng"
                    Else
                        message.InnerHtml = "Thank you for your Order. Once we Verify your Bitcoin Transfer, we'll transfer the the naira amount to your designated Bank Account below. If you have questions Regarding this transactions, please send a mail to support@btcex.com.ng"

                    End If
                Case "COMPLETED"
                    If rec.Type = "BUY" Then
                        message.InnerHtml = "Your Order is Complete!. Bitcoins has been Deposited to your Bitcoin Address. Thank you!"
                    Else
                        message.InnerHtml = "Your Order is Complete!. Money has been Deposited into your Bank Account. Thank you!"
                    End If
                Case "CANCELLED"
                    message.InnerHtml = "Your Order has been Cancelled"
            End Select

            txtbtc.InnerHtml = String.Format("{0} {1}", rec.Bitcoinamt, "BTC")
            txtcreatedon.InnerHtml = rec.Createdon.ToString
            txtnaira.InnerHtml = String.Format("{0}{1}", "₦", FormatNumber(rec.NAIRAamt, 2))
            txtrefid.InnerHtml = rec.TransGUID
            txtstatus.InnerHtml = rec.Status
            lblbuy.InnerHtml = rec.Type
            Select Case rec.Type
                Case "BUY"
                    txtdestination.InnerHtml = rec.Walletaddress

                Case "SELL"
                    txtdestination.InnerHtml = String.Format("{0} {1} ({2})", rec.Bank, rec.Accountnumber, rec.Accountname)
            End Select

        End If
    End Sub
End Class
