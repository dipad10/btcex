﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/MasterPage.master" AutoEventWireup="false" CodeFile="Allorders.aspx.vb" Inherits="Private_Allorders" %>
<%@ Register Src="~/Controls/announcement.ascx" TagName="announce" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <link href="/private/css/index.css" rel="stylesheet" media="screen" />
    <div>
        <uc1:announce ID="announce1" runat="server" />
        <h2 runat="server" id="all" class="title margin20">All Orders</h2>
        <asp:Panel ID="panelgrid" runat="server">
            <div class="panel margin20">
            <div class="content">

                <div class="panel">
                    <div class="heading bg-darkGreen">

                        <span class="title">All Transactions</span>
                    </div>
                    <div class="content">
                         <asp:Panel ID="Panel1" ScrollBars="Horizontal" Width="100%" runat="server">
                        <asp:GridView ID="GridView1" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="table striped hovered" PageSize="50" OnPageIndexChanging="GridView1_PageIndexChanging" runat="server" AllowPaging="True" OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="type" HeaderText="Type" SortExpression="type" />

                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Paymentmethod" HeaderText="Payment Method" SortExpression="Paymentmethod" />
                                <asp:TemplateField HeaderText="Bitcoin Amt">
                    <ItemTemplate>
                         <%#Eval("Bitcoinamt")%> BTC
                    </ItemTemplate>
                </asp:TemplateField>
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="NAIRAamt" DataFormatString="₦{0:#,##0}" HeaderText="Naira Amt" SortExpression="NAIRAamt" />
                                <asp:BoundField ItemStyle-CssClass="fg-darkGreen text-bold" DataField="Status" HeaderText="Status" SortExpression="Amount" />


                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Createdon" DataFormatString="{0:dd MMM yyyy}" HeaderText="Created on" SortExpression="Createdon" />
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <a href="order_details.aspx?od=<%#Eval("TransGUID")%>" class=""><i class="icon mif-search"></i></a>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                            </Columns>
                        </asp:GridView>
                             </asp:Panel>
                    </div>

                    <asp:Label ID="Label1" CssClass="text-small fg-gray text-bold" runat="server" Text=""></asp:Label>
                </div>
                <div class="align-center">

                    <asp:Button ID="createnew" Visible="true" Height="30px" OnClick="createnew_Click" CssClass="button rounded text-small fg-white bg-darkOrange fg-white" runat="server" Text="Create New" Font-Size="X-Small" Font-Bold="True" />


                </div>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:nairaConnectionString %>" SelectCommand="SELECT * FROM [Orders] WHERE ([Username] = @username) ORDER BY [Createdon] DESC">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="Username" SessionField="uname" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>



            </div>
        </div>
        </asp:Panel>

        <asp:Panel ID="paneloption" Visible="false" runat="server">
            <div class="panel margin20">
            <div class="content">

                <div class="grid">
                    <div class="row cells4">
                        <div class="cell"></div>
                        <div class="cell">
                            <a class="fg-white" href="Buy.aspx">
                                <div class="tile bg-steel fg-white" data-role="tile">
                        <div class="tile-content iconic">
                          <span class="tile-label">Buy BTC</span>
                            <span class="icon mif-coins"></span>
                        </div>
                    </div>
                            </a>
                  
            </div>
                         
                        <div class="cell">
                     <a class="fg-white" href="Sell.aspx">
                                <div class="tile bg-steel fg-white" data-role="tile">
                        <div class="tile-content iconic">
                          <span class="tile-label">Sell BTC</span>
                            <span class="icon mif-dollar"></span>
                        </div>
                    </div>
                            </a>
            </div>
                         <div class="cell"></div>
                    </div>
                </div>
                
            </div>
        </div>
        </asp:Panel>
    </div>
</asp:Content>
