﻿Imports DevExpress.Web

Partial Class Private_Payment
    Inherits System.Web.UI.Page

    Protected Sub btnconfirmpay_Click(sender As Object, e As EventArgs)
        Panelconfirm.Visible = True
        panelpreview.Visible = False
        panelsell.Visible = False

    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub

    Protected Sub btncomplete_Click(sender As Object, e As EventArgs)
        If txtdesc.Text = "" Then
            Response.Write("<script>alert('Please state a description!');</script>")
            Exit Sub
        End If
        If ASPxUploadControl1.PostedFile.FileName = "" Then

            Response.Write("<script>alert('Please Upload a screenshot (receipt, teller, bank statement etc');</script>")
            Exit Sub
        End If
        Dim numbertype As String = "paymentid"
        Dim auto As naira.Autosetting = (New cls_autosettings).Getautonumbervalue(numbertype)
        Dim ID As String = String.Format("PAYID/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)
        Dim A As New cls_paymentdetails
        Dim rec As New naira.PaymentDetail
        rec.PaymentID = ID
        rec.OrderID = Session("orderid")
        rec.UserID = Session("userid")
        rec.Description = txtdesc.Text
        rec.submittedon = DateTime.Now.ToString
        For Each file In ASPxUploadControl1.UploadedFiles
            file.SaveAs(String.Format("{0}/Private/Attach/{1}", Request.PhysicalApplicationPath, file.FileName))
            Dim bb As String = "/Private/Attach/" + file.FileName
            rec.Screenshot = file.FileName
            rec.screenshotpath = bb

        Next
        Dim res As ResponseInfo = A.Insert(rec)
        If res.ErrorCode = 0 Then
            'successs
            'get the order details concerning this particular order
            Dim b As New cls_orders
            Dim ord As naira.Order = b.SelectThisorderid(rec.OrderID)
            ord.Paymentmethod = ddltranstype.SelectedValue
            ord.Status = "AWAITING CONFIRMATION"
            b.Update(ord)
            'send the user a mail that order has been placed
            Dim body As String = mod_main.PopulateBodyorderplaced(ord.Type, ord.TransGUID, ord.Username, ord.Bitcoinamt, FormatNumber(ord.NAIRAamt, 2), ord.Createdon)
            Call mod_main.SendHtmlFormattedEmail(ord.Useremail, "", "BtcEx Order Notification!", body)
            'send mail to administrator
            Dim use As naira.User = (New cls_users).SelectThisUsername("Administrator")
            Dim adminbody As String = mod_main.PopulateBodyorderplacedadmin(ord.Type, ord.TransGUID, ord.Orderid, ord.Username, ord.Bitcoinamt, FormatNumber(ord.NAIRAamt, 2), ord.Createdon)
            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "BtcEx Order Notification!", adminbody)
            'update pament method
            Response.Redirect("/private/Order_details.aspx?od=" & ord.TransGUID & "")
        Else
            Response.Write(String.Format("<script>alert('{0} {1}');</script>", res.ErrorMessage, res.ExtraMessage))
            Exit Sub

        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim type As String = Request.QueryString("op")
            Select Case type
                Case "B"
                    'when its buying
                    panelpreview.Visible = True
                    panelsell.Visible = False
                    'get bank details setup by the admin
                    Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
                    txtacctname.Text = rec.Accountname
                    txtacctname.Enabled = False
                    txtacctnum.Text = rec.Accountnumber
                    txtacctnum.Enabled = False
                    txtbank.Text = rec.Bankname

                    txtbank.Enabled = False
                    txtguid.Text = Session("guid")
                    txtguid.Enabled = False
                    txtamount.Text = String.Format("{0}{1}", "₦", FormatNumber(Session("amount"), 2))
                    txtamount.Enabled = False
                    btnsubmit.Visible = False

                Case "S"
                    Dim A As New naira.nairaDataContext
                    Dim rec = A.GetSettings
                    Repeater1.DataSource = rec
                    Repeater1.DataBind()
                    panelsell.Visible = True
                    panelpreview.Visible = False
                    txtsellrefid.InnerHtml = Session("guid")
                    txtsellbtcamt.InnerHtml = Session("btcamount") & " BTC"
                    txtsellnairaamt.InnerText = String.Format("{0}{1}", "₦", FormatNumber(Session("amount"), 2))
            End Select

        End If
        

    End Sub

    Protected Sub btnmarksell_Click(sender As Object, e As EventArgs)
        Dim b As New cls_orders
        Dim ord As naira.Order = b.SelectThisorderid(Session("orderid"))
        ord.Status = "AWAITING CONFIRMATION"

        Try
            b.Update(ord)
            'send the user a mail that order has been placed
            Dim body As String = mod_main.PopulateBodyorderplaced(ord.Type, ord.TransGUID, ord.Username, ord.Bitcoinamt, FormatNumber(ord.NAIRAamt, 2), ord.Createdon)
            Call mod_main.SendHtmlFormattedEmail(ord.Useremail, "", "BtcEx Order Notification!", body)
            'send mail to administrator
            Dim use As naira.User = (New cls_users).SelectThisUsername("Administrator")
            Dim adminbody As String = mod_main.PopulateBodyorderplacedadmin(ord.Type, ord.TransGUID, ord.Orderid, ord.Username, ord.Bitcoinamt, FormatNumber(ord.NAIRAamt, 2), ord.Createdon)
            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "BtcEx Order Notification!", adminbody)
            'update pament method
            Response.Redirect("/private/Order_details.aspx?od=" & ord.TransGUID & "")
        Catch ex As Exception
            Response.Write(String.Format("<script>alert('{0}');</script>", ex.Message))
        End Try
    End Sub

    Protected Sub radiocredit_CheckedChanged(sender As Object, e As EventArgs)
        btnpaywithcreditcard.Visible = True
        btnsubmit.Visible = False
    End Sub

    Protected Sub radiobank_CheckedChanged(sender As Object, e As EventArgs)
        btnpaywithcreditcard.Visible = False
        btnsubmit.Visible = True
    End Sub
End Class
