﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/Admin/Admin.master" AutoEventWireup="false" CodeFile="settings.aspx.vb" Inherits="Private_Admin_settings" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <div class="panel">
    <div class="heading bg-darkGreen">
        <span class="title">Update Rates</span>
        <hr />
    </div>
    <div class="content">
        <table style="width:100%;" class="table hovered">
            <tr>
                <td><p class="text-secondary">WALLET ADDRESS:</p></td>
                <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtwalletaddress" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
                <td></td>
                <td></td>
            </tr>
              <tr>
                <td><p class="text-secondary">BUY PRICE(NGN):</p></td>
                <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtbuy" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
                <td><p class="text-secondary">SELL PRICE(NGN):</p></td>
               <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtsell" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
            </tr>
              <tr>
              
               <td><p class="text-secondary">BANK NAME:</p></td>
               
               <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtbank" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
            </tr>
              <tr>
                <td><p class="text-secondary">ACCOUNT NAME:</p></td>
                <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtacctname" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
               <td><p class="text-secondary">ACCOUNT NUMBER:</p></td>
                <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtacctnumber" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
            </tr>
        </table>
        <br />
          <h3 class="title">All Rates</h3>
        <hr />
       <dx:ASPxGridView ID="grdsettings" Styles-Header-Font-Bold="true" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="30" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource2" KeyFieldName="SN" Theme="Office2010Black" Settings-GridLines="Horizontal" EnableTheming="True" >
                   

<SettingsPager PageSize="25"></SettingsPager>

                   <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                           <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>
                    

                    

                     <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Wallet Address" FieldName="Walletaddress" VisibleIndex="2">

                       </dx:GridViewDataTextColumn>
                  
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Buy Price" EditCellStyle-CssClass="uppercase" FieldName="BuyRate" VisibleIndex="3">



                       </dx:GridViewDataTextColumn>

                      
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Sell Price" FieldName="SellRate" VisibleIndex="4">

                       </dx:GridViewDataTextColumn>
                       
                       
                       
                     <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Bankname" FieldName="Bankname" VisibleIndex="7">

                       </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Accountnumber" FieldName="Accountnumber" VisibleIndex="7">

                       </dx:GridViewDataTextColumn>
                    
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Accountname" FieldName="Accountname" VisibleIndex="7">

                       </dx:GridViewDataTextColumn>
                    
                   </Columns>

                  <Styles>
<Header Font-Bold="True"></Header>

            <AlternatingRow Enabled="true" />
                     
<SelectedRow ForeColor="White"></SelectedRow>
                     
        </Styles>
               </dx:ASPxGridView>
                        

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:nairaconnectionstring %>" SelectCommand="SELECT * FROM [Settings]"></asp:SqlDataSource>
                  <center>
           <asp:Button ID="btnsubmit" Visible="true" Height="40px" OnClick="btnsubmit_Click" CssClass="button align-center bg-orange text-secondary fg-white rounded" runat="server" Text="Save" Font-Bold="True" />      

    </center>      
        </div>
         </div>
   
</asp:Content>

