﻿
Partial Class Private_Admin_Announcement
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs)
        Dim a As New cls_announcement
        Dim rec As naira.Announcement = a.SelectThisID(1)
        rec.Announcement = txtannouncement.Text
        Try
            a.Update(rec)
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Saved Succesfully');window.location='announcement.aspx';", True)

        Catch ex As Exception
            Response.Write("<script>alert('" & ex.Message & "');</script>")
            Exit Sub

        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim rec As naira.Announcement = (New cls_announcement).SelectThisID(1)
            txtannouncement.Text = rec.Announcement

        End If
    End Sub
End Class
