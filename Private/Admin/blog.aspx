﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/Admin/Admin.master" AutoEventWireup="false" CodeFile="blog.aspx.vb" Inherits="Private_Admin_blog" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <div class="panel">
        <div class="heading bg-darkGreen">
            <span class="title">Blog</span>
            <hr />
        </div>
        <div class="content">
            <table style="width: 100%;" class="table hovered">
                <tr>
                    <td>
                        <p class="text-secondary">Post Title:</p>
                    </td>
                    <td>
                        <span class="input-control text">
                            <asp:TextBox ID="txttitle" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                    </td>
                    <td>
                        <p class="text-secondary">Short Desc:</p>
                    </td>
                    <td>
                        <span class="input-control texte">
                            <asp:TextBox ID="txtsdesc" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="text-secondary">Full Desc:</p>
                    </td>
                    <td>
                        <span class="input-control text">
                            <asp:TextBox ID="txtfdesc" TextMode="MultiLine" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                    </td>
                    <td>
                        <p class="text-secondary">Image:</p>
                    </td>
                    <td>
                        <span class="input-control text full-size">
                            <dx:ASPxUploadControl ID="ASPxUploadControl1" NullText="Upload Screenshot" CssClass="shadow" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server" UploadMode="Standard">
                                <ValidationSettings AllowedFileExtensions=".txt,.jpg,.jpe,.jpeg,.doc,.png" MaxFileSize="1000000">
                                </ValidationSettings>
                            </dx:ASPxUploadControl>
                        </span>
                        <span>
                            <asp:Image ID="Image1" Visible="false" Width="80" Height="80" runat="server" />
                                </span>
                    </td>
                </tr>
            </table>
           <div class="align-center">

                    <asp:Button ID="Submit" Visible="true" Height="30px" OnClick="Submit_Click" CssClass="button rounded text-small fg-white bg-darkOrange fg-white" runat="server" Text="Save" Font-Size="X-Small" Font-Bold="True" />


                </div>
            <br />
            <dx:ASPxGridView ID="grdsettings" Styles-Header-Font-Bold="true" Width="100%" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="30" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Theme="Office2010Black" Settings-GridLines="Horizontal" EnableTheming="True">


                <SettingsPager PageSize="25"></SettingsPager>

                <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                <SettingsSearchPanel Visible="True" />
                <Columns>
                    <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0" ShowDeleteButton="True">
                    </dx:GridViewCommandColumn>

                    <dx:GridViewDataHyperLinkColumn FieldName="id" PropertiesHyperLinkEdit-Style-CssClass="text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="id" PropertiesHyperLinkEdit-NavigateUrlFormatString="blog.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="blog.aspx?edit-id={0}" TextFormatString="Orderid">
                            <Style CssClass=" text-small text-bold"></Style>
                        </PropertiesHyperLinkEdit>

                    </dx:GridViewDataHyperLinkColumn>

                    <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Short Description" FieldName="sdesc" VisibleIndex="1">
<CellStyle CssClass="text-small"></CellStyle>
                    </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Full Description" FieldName="fdesc" VisibleIndex="3">
<CellStyle CssClass="text-small"></CellStyle>
                    </dx:GridViewDataTextColumn>


                    <dx:GridViewDataDateColumn FieldName="date" VisibleIndex="2">
                    </dx:GridViewDataDateColumn>




                  
                </Columns>

                <Styles>
                    <Header Font-Bold="True"></Header>

                    <AlternatingRow Enabled="true" />

                    <SelectedRow ForeColor="White"></SelectedRow>

                </Styles>
            </dx:ASPxGridView>


            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:nairaConnectionString %>" SelectCommand="SELECT * FROM [blog] ORDER BY date DESC"></asp:SqlDataSource>
            <center>

    </center>
        </div>
    </div>
</asp:Content>

