﻿Imports DevExpress.Web

Partial Class Private_Admin_blog
    Inherits System.Web.UI.Page

    Protected Sub Submit_Click(sender As Object, e As EventArgs)
        If ASPxUploadControl1.PostedFile.FileName = "" Then
            Response.Write("<script>alert('Please upload a picture');</script>")
            Exit Sub

        End If
        Dim blogid As String = Request.QueryString("edit-id")
        Dim A As New cls_blog
        If blogid <> "" Then
            'update
            Dim rec As naira.blog = A.SelectThisID(blogid)
            rec.title = txttitle.Text
            rec.fdesc = txtfdesc.Text
            rec.sdesc = txtsdesc.Text
            rec.bywho = Session("uname")
            rec.date = Date.Now
            If ASPxUploadControl1.PostedFile.FileName <> "" Then

                For Each file In ASPxUploadControl1.UploadedFiles
                    file.SaveAs(String.Format("{0}/blog/attach/{1}", Request.PhysicalApplicationPath, file.FileName))
                    Dim bb As String = "/blog/attach/" + file.FileName
                    rec.image = bb
                Next

            End If
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                'successful
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved Successfully');window.location='blog.aspx';", True)
            Else
                Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
            End If
        Else
            'create new instance
            Dim rec As New naira.blog
            rec.title = txttitle.Text
            rec.fdesc = txtfdesc.Text
            rec.sdesc = txtsdesc.Text
            rec.bywho = Session("uname")
            rec.date = Date.Now
            If ASPxUploadControl1.PostedFile.FileName <> "" Then

                For Each file In ASPxUploadControl1.UploadedFiles
                    file.SaveAs(String.Format("{0}/blog/attach/{1}", Request.PhysicalApplicationPath, file.FileName))
                    Dim bb As String = "/blog/attach/" + file.FileName
                    rec.image = bb
                Next

            End If
            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then
                'successful
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved Successfully');window.location='blog.aspx';", True)
            Else
                Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim blogid As String = Request.QueryString("edit-id")
            If blogid <> "" Then
                Dim A As New cls_blog
                Dim rec As naira.blog = A.SelectThisID(blogid)
                txttitle.Text = rec.title
                txtfdesc.Text = rec.fdesc
                txtsdesc.Text = rec.sdesc
                Image1.ImageUrl = rec.image

            End If
        End If
    End Sub

    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub
End Class
