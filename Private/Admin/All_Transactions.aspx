﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/Admin/Admin.master" AutoEventWireup="false" CodeFile="All_Transactions.aspx.vb" Inherits="Private_Admin_All_Transactions" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <h3 class="title">All Transactions</h3>
       <dx:ASPxGridView ID="grdtransactions" Styles-Header-Font-Bold="true" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="30" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource2" KeyFieldName="SN" Theme="Office2010Black" Settings-GridLines="Horizontal" EnableTheming="True" >
                   

<SettingsPager PageSize="25"></SettingsPager>

                   <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                           <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>
                      <dx:GridViewDataHyperLinkColumn FieldName="Orderid" PropertiesHyperLinkEdit-Style-CssClass=" text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="Orderid" PropertiesHyperLinkEdit-NavigateUrlFormatString="orders_edit.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="orders_edit.aspx?edit-id={0}" TextFormatString="Orderid">
<Style CssClass=" text-small text-bold"></Style>
                        </PropertiesHyperLinkEdit>

                       </dx:GridViewDataHyperLinkColumn>

                        <dx:GridViewDataHyperLinkColumn FieldName="Userid" PropertiesHyperLinkEdit-Style-CssClass="text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="Userid" PropertiesHyperLinkEdit-NavigateUrlFormatString="users_edit.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="users_edit.aspx?edit-id={0}" TextFormatString="Userid">
<Style CssClass=" text-small text-bold"></Style>
                        </PropertiesHyperLinkEdit>

                       </dx:GridViewDataHyperLinkColumn>

                     <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Type" FieldName="Type" VisibleIndex="2">

                       </dx:GridViewDataTextColumn>
                  
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Userfullname" EditCellStyle-CssClass="uppercase" FieldName="Userfullname" VisibleIndex="3">



                       </dx:GridViewDataTextColumn>

                      
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Bitcoin Amount(BTC)" FieldName="Bitcoinamt" VisibleIndex="4">

                       </dx:GridViewDataTextColumn>
                       
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="NairaAmount(NGN)" PropertiesTextEdit-DisplayFormatString="₦{0:#,##0}" FieldName="NAIRAamt" VisibleIndex="5">

                       </dx:GridViewDataTextColumn>
                       
                     <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" Caption="Status" FieldName="Status" VisibleIndex="7">

                       </dx:GridViewDataTextColumn>
                       <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small" FieldName="Createdon" VisibleIndex="8">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

                       </dx:GridViewDataDateColumn>
                   
                   </Columns>

                  <Styles>
<Header Font-Bold="True"></Header>

            <AlternatingRow Enabled="true" />
                     
<SelectedRow ForeColor="White"></SelectedRow>
                     
        </Styles>
               </dx:ASPxGridView>
                        

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:nairaconnectionstring %>" SelectCommand="SELECT * FROM [Orders] ORDER BY [createdon] DESC"></asp:SqlDataSource>
                        

</asp:Content>

