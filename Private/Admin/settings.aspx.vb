﻿
Partial Class Private_Admin_settings
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs)
        Dim A As New cls_settings
        Dim rec As naira.Setting = A.SelectThisID(1)
        rec.Walletaddress = txtwalletaddress.Text

        rec.SellRate = txtsell.Text
        rec.BuyRate = txtbuy.Text
        rec.Bankname = txtbank.Text
        rec.Accountnumber = txtacctnumber.Text
        rec.Accountname = txtacctname.Text
        Try
            A.Update(rec)
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Saved Succesfully');window.location='settings.aspx';", True)

        Catch ex As Exception
            Response.Write("<script>alert('" & ex.Message & "');</script>")
            Exit Sub

        End Try

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
            txtwalletaddress.Text = rec.Walletaddress
            txtacctname.Text = rec.Accountname
            txtacctnumber.Text = rec.Accountnumber
            txtbank.Text = rec.Bankname
            'txtbtcvalue.Text = rec.TodaybtcValue
            txtbuy.Text = rec.BuyRate
            txtsell.Text = rec.SellRate

        End If
    End Sub
End Class
