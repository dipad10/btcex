﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/Admin/Admin.master" AutoEventWireup="false" CodeFile="Announcement.aspx.vb" Inherits="Private_Admin_Announcement" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
      <div class="panel">
    <div class="heading bg-darkGreen">
        <span class="title">Annoucements</span>
        <hr />
    </div>
    <div class="content">
         <table style="width:100%;" class="table hovered">
            <tr>
                <td><p class="text-secondary">ANNOUNCEMENT:</p></td>
                <td>
                       <span class="input-control text full-size">
                                      <asp:TextBox ID="txtannouncement" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                </td>
                <td></td>
                <td></td>
            </tr>
             </table>
        <br />
       <dx:ASPxGridView ID="grdsettings" Styles-Header-Font-Bold="true" Width="100%" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="30" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource2" Theme="Office2010Black" Settings-GridLines="Horizontal" EnableTheming="True" >
                   

<SettingsPager PageSize="25"></SettingsPager>

                   <Settings ShowFilterBar="Auto" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                           <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
                           </dx:GridViewCommandColumn>
                  
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" EditCellStyle-CssClass="uppercase" FieldName="Announcement" VisibleIndex="1">



                       </dx:GridViewDataTextColumn>

                      
                           <dx:GridViewDataDateColumn FieldName="submittedon" VisibleIndex="2">
                           </dx:GridViewDataDateColumn>
                    

                    

                     <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="submittedby" VisibleIndex="3">

                       </dx:GridViewDataTextColumn>
                  
                   </Columns>

                  <Styles>
<Header Font-Bold="True"></Header>

            <AlternatingRow Enabled="true" />
                     
<SelectedRow ForeColor="White"></SelectedRow>
                     
        </Styles>
               </dx:ASPxGridView>
                        

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:nairaConnectionString %>" SelectCommand="SELECT [Announcement], [submittedon], [submittedby] FROM [Announcement]"></asp:SqlDataSource>
                        <center>
           <asp:Button ID="btnsubmit" Visible="true" Height="40px" OnClick="btnsubmit_Click" CssClass="button align-center bg-orange text-secondary fg-white rounded" runat="server" Text="Save" Font-Bold="True" />      

    </center>
        </div>
         </div>
</asp:Content>

