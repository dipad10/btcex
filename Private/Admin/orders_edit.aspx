﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/Admin/Admin.master" AutoEventWireup="false" CodeFile="orders_edit.aspx.vb" Inherits="Private_Admin_orders_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    
      <div class="panel">
    <div class="heading bg-darkGreen">
        <span class="title">Order Information</span>
        <hr />
    </div>
    <div class="content">
        <table style="width:100%;">
            <tr>
                <td><p class="text-secondary text-bold">Order ID:</p></td>
                 <td>
                     <span runat="server" id="lblorderid" class="text-secondary"></span>
                 </td>
            </tr>
      <tr>
                <td><p class="text-secondary text-bold">Userfullname:</p></td>
                 <td>
                     <span runat="server" id="lblfullname" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">User Email:</p></td>
                 <td>
                     <span runat="server" id="lblemail" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Bitcoin Amount:</p></td>
                 <td>
                     <span runat="server" id="Bitcoinamt" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Naira Amount:</p></td>
                 <td>
                     <span runat="server" id="nairaamt" class="text-secondary"></span>
                 </td>
            </tr>
             <tr>
                <td><p class="text-secondary text-bold">Tranaction Type:</p></td>
                 <td>
                     <span runat="server" id="type" class="text-secondary text-bold text-shadow fg-green"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Bitcoin Wallet Address:</p></td>
                 <td>
                     <span runat="server" id="walletaddress" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Bank:</p></td>
                 <td>
                     <span runat="server" id="Bank" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Accountname:</p></td>
                 <td>
                     <span runat="server" id="accountname" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Accountnumber:</p></td>
                 <td>
                     <span runat="server" id="accountnumber" class="text-secondary"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Ref ID:</p></td>
                 <td>
                     <span runat="server" id="refid" class="text-secondary"></span>
                 </td>
            </tr>

              <tr>
                <td><p class="text-secondary text-bold">Status:</p></td>
                 <td>
                     <span runat="server" id="txtstatus" class="text-secondary text-bold text-shadow fg-darkBrown"></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-secondary text-bold">Created On:</p></td>
                 <td>
                     <span runat="server" id="createdon" class="text-secondary"></span>
                 </td>
            </tr>
        </table>
        <br />
        <asp:Panel ID="Panel1" runat="server"></asp:Panel>
        <h4 class="title">Payment Details/proof</h4>
        <hr />
         <asp:DataList ID="DataList1" runat="server">
                     <ItemTemplate>
        <table style="width:100%;">
             <tr>
                <td><p class="text-secondary text-bold">Payment ID:</p></td>
                 <td>
                     <span class="text-secondary"><%#Eval("PaymentID")%></span>
                 </td>
            </tr>

             <tr>
                <td><p class="text-secondary text-bold">Description:</p></td>
                 <td>
                     <span class="text-secondary"><%#Eval("Description")%></span>
                 </td>
            </tr>
             <tr>
                
                           <td><p class="text-secondary text-bold">Payment Screenshot:</p></td>
                 <td>
                     <a href="<%#Eval("screenshotpath")%>" download="<%#Eval("screenshot")%>"><img width="200" height="200" src="<%#Eval("screenshotpath")%>" /></a>
                 </td>
                    
              
            </tr>
        </table>
                          </ItemTemplate>
                 </asp:DataList>
        <center>
           <asp:Button ID="btnsubmit" Visible="true" Height="40px" OnClick="btnsubmit_Click" CssClass="button align-center bg-orange text-secondary fg-white rounded" runat="server" Text="Confirm/Complete Order" Font-Bold="True" />      

    </center>
        </div>
          </div>
     

</asp:Content>

