﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/Admin/Admin.master" AutoEventWireup="false" CodeFile="manageusers.aspx.vb" Inherits="Private_Admin_manageusers" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <h3 class="title">All Users</h3>
       <dx:ASPxGridView ID="grdusers" Styles-Header-Font-Bold="true" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="30" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource2" Theme="Office2010Black" Settings-GridLines="Horizontal" EnableTheming="True" >
                   

<SettingsPager PageSize="25"></SettingsPager>

                   <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                         <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>
                         <dx:GridViewDataHyperLinkColumn FieldName="UserID" PropertiesHyperLinkEdit-Style-CssClass="text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="UserID" PropertiesHyperLinkEdit-NavigateUrlFormatString="users_edit.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="users_edit.aspx?edit-id={0}" TextFormatString="UserID">
<Style CssClass=" text-small text-bold"></Style>
                        </PropertiesHyperLinkEdit>

                       </dx:GridViewDataHyperLinkColumn>
                    
                  
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" EditCellStyle-CssClass="uppercase" FieldName="Username" VisibleIndex="2">



                       </dx:GridViewDataTextColumn>

                      
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Fullname" VisibleIndex="3">

                       </dx:GridViewDataTextColumn>
                       
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" PropertiesTextEdit-DisplayFormatString="₦{0:#,##0}" FieldName="Address" VisibleIndex="4">

                       </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Email" VisibleIndex="5">

                       </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Phone" VisibleIndex="6">

                       </dx:GridViewDataTextColumn>
                       <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small" FieldName="SubmittedOn" VisibleIndex="7">

                       </dx:GridViewDataDateColumn>
                   
                           <dx:GridViewDataTextColumn FieldName="permission" VisibleIndex="8">
                           </dx:GridViewDataTextColumn>
                   
                   </Columns>

                  <Styles>
<Header Font-Bold="True"></Header>

            <AlternatingRow Enabled="true" />
                     
<SelectedRow ForeColor="White"></SelectedRow>
                     
        </Styles>
               </dx:ASPxGridView>
                        

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:nairaConnectionString %>" SelectCommand="SELECT [UserID], [Username], [Fullname], [Address], [Email], [Phone], [SubmittedOn], [permission] FROM [Users] ORDER BY [SubmittedOn] DESC"></asp:SqlDataSource>
                        

</asp:Content>

