﻿
Partial Class Private_Admin_orders_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim order As String = Request.QueryString("edit-id")
            Dim rec As naira.Order = (New cls_orders).SelectThisorderid(order)
            lblorderid.InnerHtml = rec.Orderid
            lblfullname.InnerHtml = rec.Userfullname
            lblemail.InnerHtml = rec.Useremail
            Bitcoinamt.InnerHtml = String.Format("{0} {1}", rec.Bitcoinamt, "BTC")
            nairaamt.InnerHtml = String.Format("{0}{1}", "₦", FormatNumber(rec.NAIRAamt, 2))
            type.InnerHtml = rec.Type
            Bank.InnerHtml = rec.Bank
            txtstatus.InnerHtml = rec.Status
            accountnumber.InnerHtml = rec.Accountnumber
            accountname.InnerHtml = rec.Accountname
            refid.InnerHtml = rec.TransGUID
            createdon.InnerHtml = rec.Createdon
            walletaddress.InnerHtml = rec.Walletaddress
            Dim reclist As List(Of naira.PaymentDetail) = (New cls_paymentdetails).SelectThisorderlist(order)
            DataList1.DataSource = reclist
            DataList1.DataBind()

        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs)

    End Sub
End Class
