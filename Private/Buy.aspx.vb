﻿Imports System.Net

Partial Class Private_Buy
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs)

        If calc.getnairaValue = "" Or calc.getbtcValue = 0 Or calc.getbtcValue = "" Then
            Response.Write("<script>alert('Naira Amount cannot be 0 or empty, Please Insert your Btc Amount and click Get Price');</script>")
            Exit Sub
        End If
        If calc.getnairaValue < 10000 Then
            Response.Write("<script>alert('OOps! Sorry You can only transact N10,000 and above!');</script>")
            Exit Sub

        End If
        If txtwallet.Text = "" Then
            Response.Write("<script>alert('Please type in your Bitcoin Wallet address!');</script>")
            Exit Sub
        End If
        Dim A As New cls_orders

        Dim rec As New naira.Order
        Dim numbertype As String = "orderid"
        Dim auto As naira.Autosetting = (New cls_autosettings).Getautonumbervalue(numbertype)
        Dim ID As String = String.Format("ODR/BUY/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)

        rec.Username = (Session("uname"))
        rec.NAIRAamt = calc.getnairaValue
        rec.Bitcoinamt = calc.getbtcValue

        rec.fullamount = String.Format("{0} for {1}", rec.Bitcoinamt, rec.NAIRAamt)
        rec.Orderid = ID
        rec.TransGUID = System.Guid.NewGuid.ToString
        rec.Status = "PENDING"
        rec.Type = "BUY"
        rec.Paymentmethod = "TBA"
        rec.Duecanceldate = DateTime.Now.AddHours(2)
        rec.Createdon = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        rec.Walletaddress = txtwallet.Text
        rec.Active = 1

        Dim use As naira.User = (New cls_users).SelectThisUsername(rec.Username)
        rec.Userid = use.UserID
        rec.Useremail = use.Email
        rec.Userfullname = use.Fullname.ToUpper
        Dim res As ResponseInfo = A.Insert(rec)
        If res.ErrorCode = 0 Then
            'successful
            Session("orderid") = rec.Orderid
            Session("userid") = rec.Userid
            Session("guid") = rec.TransGUID
            Session("amount") = rec.NAIRAamt
            Response.Redirect("/private/payment.aspx?op=B")
        Else
            Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
            Exit Sub

        End If



    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            'Dim user As naira.User = (New cls_users).SelectThisUsername(Session("uname"))
            'If user.Verified = 0 Then
            '    'Response.Write("<script>alert('Your account has not been verified, Please upload a Government Issued ID to verify your Account e.g (Driver's Licence, National ID E.t.c)!');</script>")
            '    'Response.Redirect("/private/profile/profile_edit.aspx")
            '    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Please Verify your Account, upload valid ID documents');window.location='profile/profile_edit.aspx';", True)

            'Else
            Dim todaybtc As Double = 0
            Try
                Dim uri = [String].Format("https://blockchain.info/tobtc?currency=USD&value={0}", "1")

                Dim client As New WebClient()
                client.UseDefaultCredentials = True
                Dim data = client.DownloadString(uri)

                Dim result = Convert.ToDouble(data)
                todaybtc = FormatNumber(1 / result, 2)
                Dim rec As naira.Setting = (New cls_settings).SelectThisID(1)
                Dim buyprice As Double = 0


                buyprice = todaybtc * rec.BuyRate
                lblrate.InnerHtml = String.Format("RATE : 1BTC = {0}{1}", "₦", FormatNumber(buyprice, 2))
            Catch ex As Exception
                todaybtc = 0
            End Try

            'End If

        End If
    End Sub
End Class
