﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/MasterPage.master" AutoEventWireup="false" CodeFile="profile_edit.aspx.vb" Inherits="Private_profile_profile_edit" %>
<%@ Register Src="~/Controls/announcement.ascx" TagName="announce" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <link href="/private/css/index.css" rel="stylesheet" media="screen" />
    <div>
        <uc1:announce ID="announce1" runat="server" />
        <h2 runat="server" id="all" class="title margin20">Profile Details</h2>
        <asp:Panel ID="panel1" runat="server">
            <div class="panel margin20">
            <div class="content">
                <h5 class="hint-title">Personal Information</h5>
            <hr />
                <div>
                    <table class="hovered" width="100%">
                        <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Full name: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtfullname" Enabled="false" CssClass="text-secondary uppercase text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                          <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Username: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtusername" Enabled="false" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                         <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Email: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtemail" CssClass="text-secondary uppercase text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                          <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Password: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtpassword" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                         <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Phone number: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtphonenumber" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                        <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Address: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtaddress" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>

                         <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Means Of Identification: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">

                                      <asp:DropDownList ID="ddlmoid" runat="server">
                                         
                                           <asp:ListItem Value="National ID">National ID</asp:ListItem>
                            <asp:ListItem Value="Driver's Licence">Driver's Licence</asp:ListItem>
                            <asp:ListItem Value="International Passport">International Passport</asp:ListItem>
                            <asp:ListItem Value="Permanent Voters Card">Permanent Voters Card</asp:ListItem>
                                      </asp:DropDownList>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                          <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Upload*: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <dx:ASPxUploadControl ID="ASPxUploadControl1" NullText="Upload Screenshot" CssClass="shadow" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server" UploadMode="Standard">
                                <ValidationSettings AllowedFileExtensions=".txt,.jpg,.jpe,.jpeg,.doc,.png" MaxFileSize="1000000">
                                </ValidationSettings>
                            </dx:ASPxUploadControl>
                                     
                        </span> <span>
                            <asp:Image ID="Image1" Visible="false" Width="80" Height="80" runat="server" />
                                </span>
                              </td>
                              <td>
                                  
                              </td>
                              <td></td>
                        </tr>
                          <tr>
                            <td>
                                  <p class="text-secondary uppercase text-bold">Created Date: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtcreatedon" Enabled="false" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                    </table>
                </div>
                <div class="align-center">

                    <asp:Button ID="Submit" Visible="true" Height="30px" OnClick="Submit_Click" CssClass="button rounded text-small fg-white bg-darkOrange fg-white" runat="server" Text="Save" Font-Size="X-Small" Font-Bold="True" />


                </div>
             


            </div>
        </div>
        </asp:Panel>
 

        
    </div>
</asp:Content>
