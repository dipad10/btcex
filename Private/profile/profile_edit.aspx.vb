﻿Imports DevExpress.Web

Partial Class Private_profile_profile_edit
    Inherits System.Web.UI.Page

    Protected Sub Submit_Click(sender As Object, e As EventArgs)
        Dim A As New cls_users
        Dim rec As naira.User = A.SelectThisUsername(Session("uname"))
        rec.Address = txtaddress.Text
        rec.Email = txtemail.Text
        rec.Password = txtpassword.Text
        rec.Phone = txtphonenumber.Text
        rec.MeansofID = ddlmoid.SelectedValue
        If ASPxUploadControl1.PostedFile.FileName <> "" Then

            For Each file In ASPxUploadControl1.UploadedFiles
                file.SaveAs(String.Format("{0}/Private/AttachMOID/{1}", Request.PhysicalApplicationPath, file.FileName))
                Dim bb As String = "/Private/AttachMOID/" + file.FileName
                rec.idpicture = file.FileName
                rec.Idpath = bb
                rec.Verified = 1
            Next

        End If
        Dim res As ResponseInfo = A.Update(rec)
        If res.ErrorCode = 0 Then
            'successful
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved Successfully');window.location='profile_edit.aspx';", True)
        Else
            Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
        End If
    End Sub


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim rec As naira.User = (New cls_users).SelectThisUsername(Session("uname"))
            txtaddress.Text = rec.Address
            txtcreatedon.Text = rec.SubmittedOn
            txtemail.Text = rec.Email
            txtfullname.Text = rec.Fullname.ToUpper
            txtpassword.Text = rec.Password
            txtphonenumber.Text = rec.Phone
            txtusername.Text = rec.Username
            ddlmoid.SelectedValue = rec.MeansofID
            Image1.ImageUrl = rec.Idpath
            Image1.Visible = True

        End If
    End Sub
    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub

End Class
