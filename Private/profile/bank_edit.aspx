﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Private/MasterPage.master" AutoEventWireup="false" CodeFile="bank_edit.aspx.vb" Inherits="Private_profile_bank_edit" %>
<%@ Register Src="~/Controls/announcement.ascx" TagName="announce" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <link href="/private/css/index.css" rel="stylesheet" media="screen" />
    <div>
        <uc1:announce ID="announce1" runat="server" />
        <h2 runat="server" id="all" class="title margin20">Setup Bank Details</h2>
        <asp:Panel ID="panel1" runat="server">
            <div class="panel margin20">
            <div class="content">
                <h5 class="hint-title">Payment Information</h5>
            <hr />
                <div>
                    <table width="100%">
                        <tr>
                            <td>
                                  <p class="text-secondary text-bold">Bank Name: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtbankname" CssClass="text-secondary uppercase text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                         <tr>
                            <td>
                                  <p class="text-secondary text-bold">Account Name: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtacctname" CssClass="text-secondary uppercase text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                         <tr>
                            <td>
                                  <p class="text-secondary text-bold">Account Number: </p>
                            </td>
                              <td>
                                  <span class="input-control text full-size">
                                      <asp:TextBox ID="txtacctnumber" CssClass="text-secondary text-bold" runat="server"></asp:TextBox>
                        </span>
                              </td>
                              <td></td>
                              <td></td>
                        </tr>
                    </table>
                </div>
                <div class="align-center">

                    <asp:Button ID="Submit" Visible="true" Height="30px" OnClick="Submit_Click" CssClass="button rounded text-small fg-white bg-darkOrange fg-white" runat="server" Text="Save" Font-Size="X-Small" Font-Bold="True" />


                </div>
             


            </div>
        </div>
        </asp:Panel>
        <asp:Panel ID="panelgrid" runat="server">
            <div class="panel margin20">
            <div class="content">

                <div class="panel">
                    <div class="heading bg-darkGreen">

                        <span class="title">Bank Payment</span>
                    </div>
                    <div class="content">
                         <asp:Panel ID="Panel2" ScrollBars="Horizontal" Width="100%" runat="server">
                        <asp:GridView ID="GridView1" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="table striped hovered" PageSize="50" OnPageIndexChanging="GridView1_PageIndexChanging" runat="server" AllowPaging="True" OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="bankname" HeaderText="BANK" SortExpression="bankname" />

                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Accountname" HeaderText="ACCOUNT NAME" SortExpression="Accountname" />
                               
                                <asp:BoundField ItemStyle-CssClass="fg-darkGreen text-bold" DataField="Accountnumber" HeaderText="ACCOUNT NUMBER" SortExpression="Accountnumber" />


                                <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                            </Columns>
                        </asp:GridView>
                             </asp:Panel>
                    </div>

                    <asp:Label ID="Label1" CssClass="text-small fg-gray text-bold" runat="server" Text=""></asp:Label>
                </div>
                
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:nairaConnectionString %>" SelectCommand="SELECT * FROM [users] WHERE ([Username] = @username)">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="Username" SessionField="uname" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>



            </div>
        </div>
        </asp:Panel>

        
    </div>
</asp:Content>

