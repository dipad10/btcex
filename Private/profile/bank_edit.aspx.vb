﻿
Partial Class Private_profile_bank_edit
    Inherits System.Web.UI.Page

    Protected Sub Submit_Click(sender As Object, e As EventArgs)
        Dim A As New cls_users
        Dim rec As naira.User = A.SelectThisUsername(Session("uname"))
        rec.Bankname = txtbankname.Text.ToUpper
        rec.Accountnumber = txtacctnumber.Text
        rec.Accountname = txtacctname.Text.ToUpper
        Dim res As ResponseInfo = A.Update(rec)
        If res.ErrorCode = 0 Then
            'successful
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved Successfully');window.location='bank_edit.aspx';", True)
        Else
            Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
        End If

    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)

    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If GridView1.Rows.Count = 0 Then
            Label1.Text = "No orders Found"
        Else
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        If Not Page.IsPostBack Then
            Dim rec As naira.User = (New cls_users).SelectThisUsername(Session("uname"))
            txtacctname.Text = rec.Accountname
            txtacctnumber.Text = rec.Accountnumber
            txtbankname.Text = rec.Bankname

        End If
    End Sub
End Class
