﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Login
    Inherits System.Web.UI.Page
    Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("nairaConnectionString").ConnectionString)
    'Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
    '    If _Connection.State = ConnectionState.Open Then
    '        _Connection.Close()
    '    End If

    '    If Not Page.IsPostBack Then

    '        Dim message As String = Request.QueryString("msg")
    '        If message <> "" Then
    '            Response.Write("<script>alert('You have logged out successfully');</script>")
    '            Session("uname") = ""


    '        End If


    '    End If
    'End Sub

    Protected Sub Btnsignin_Click(sender As Object, e As EventArgs)
        If txtemail.Text = "" Then
            messagebox.Visible = True
            errortext.InnerHtml = "Email is required"
            Exit Sub
        End If

        If txtpassword.Text = "" Then
            messagebox.Visible = True
            errortext.InnerHtml = "Password is required"
            Exit Sub
        End If
        Dim rec As List(Of naira.User) = (New cls_users).Authenticatelogin(txtemail.Text, txtpassword.Text)
        If rec.Count = 0 Then
            'invalid user
            messagebox.Visible = True
            errortext.InnerHtml = "Invalid username or Password"

            Exit Sub

        Else
            Try
                Dim user As naira.User = (New cls_users).SelectThisemail(txtemail.Text)

                Session("uname") = user.Username
                Select Case user.permission
                    Case "USER"
                        Response.Redirect("~/Private/Dashboard.aspx")
                    Case "ADMIN"
                        Response.Redirect("~/Private/Admin/Dashboard.aspx")
                End Select

                ''Response.Redirect("")
            Catch ex As Exception
                messagebox.Visible = True
                errortext.InnerHtml = ex.Message
            End Try

        End If
        'Try
        '    Dim path As String = Request.QueryString("goto")
        '    If path <> "" Then
        '        'check if its a link to check ticket from email
        '        _Connection.Open()
        '        Dim _cmd As SqlCommand = _Connection.CreateCommand()
        '        _cmd.CommandType = CommandType.Text
        '        _cmd.CommandText = "select * from Users where Email=( '" + txtusername.Text + "') and Password=( '" + mod_main.Encrypt(txtpassword.Text) + "')"
        '        _cmd.ExecuteNonQuery()
        '        Dim dr As SqlDataReader
        '        dr = _cmd.ExecuteReader()
        '        If dr.Read() Then
        '            Session("uname") = dr("Username")
        '            Session("password") = txtpassword.Text
        '            Session("datetime") = Date.Now.ToString

        '            Dim A As New cls_users
        '            Dim rec As GHD5.User = A.SelectThisemail(txtusername.Text)
        '            rec.LastLoginDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        '            Dim res As ResponseInfo = A.Update(rec)
        '            If res.ErrorCode = 0 Then
        '                'Call mod_main.InsertActivity("LOGGED IN", "SERVICEDESK", "TICKET", "" & Session("uname") & "Logged In", Session("uname"), "")
        '                Response.Redirect(path)

        '            Else

        '                Response.Write("<script>alert('" & res.ErrorCode & " " & res.ExtraMessage & "');</script>")
        '            End If

        '            'If dr("permission").ToString() = "Admin" Then
        '            '    Session("datetime") = Date.Now.ToString
        '            '    Response.Redirect("/Webroot/Admin/Dashboard.aspx")

        '            'ElseIf dr("permission").ToString() = "BasicUser" Then
        '            '    Session("datetime") = Date.Now.ToString
        '            '    Response.Redirect("/Webroot/Dashboard.aspx")

        '            'End If
        '        Else
        '            Response.Write("<script>alert('Invalid username or Password');</script>")
        '        End If

        '        _Connection.Close()
        '        _Connection.Dispose()
        '        dr.Close()
        '    Else
        '        'normal login
        '        _Connection.Open()
        '        Dim _cmd As SqlCommand = _Connection.CreateCommand()
        '        _cmd.CommandType = CommandType.Text
        '        _cmd.CommandText = "select * from Users where Email=( '" + txtusername.Text + "') and Password=( '" + mod_main.Encrypt(txtpassword.Text) + "')"
        '        _cmd.ExecuteNonQuery()
        '        Dim dr As SqlDataReader
        '        dr = _cmd.ExecuteReader()
        '        If dr.Read() Then
        '            Session("uname") = dr("Username")
        '            Session("password") = txtpassword.Text
        '            'save last login date

        '            Dim A As New cls_users
        '            Dim rec As GHD5.User = A.SelectThisemail(txtusername.Text)
        '            rec.LastLoginDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        '            Dim res As ResponseInfo = A.Update(rec)
        '            If res.ErrorCode = 0 Then
        '                'Call mod_main.InsertActivity("LOGGED IN", "SERVICEDESK", "TICKET", "" & Session("uname") & "Logged In", Session("uname"), "")
        '                If dr("permission").ToString() = "Admin" Then
        '                    Session("datetime") = Date.Now.ToString
        '                    Response.Redirect("/Webroot/Admin/Home.aspx")

        '                ElseIf dr("permission").ToString() = "BasicUser" Then
        '                    Session("datetime") = Date.Now.ToString
        '                    Response.Redirect("/Webroot/Helpdesk/Ticket_new.aspx")

        '                End If

        '            Else
        '                Response.Write("<script>alert('" & res.ErrorMessage & " " & res.ExtraMessage & "');</script>")
        '                Exit Sub

        '            End If


        '        Else
        '            Response.Write("<script>alert('Invalid username or Password');</script>")
        '        End If

        '        _Connection.Close()
        '        _Connection.Dispose()
        '        dr.Close()

        '    End If

        'Catch ex As Exception
        '    Response.Write("<script>alert('An error Occured: " + ex.Message + "');</script>")
        'End Try

    End Sub

   
End Class
