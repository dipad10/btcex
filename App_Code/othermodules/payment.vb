﻿Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json

Namespace amplifypayment
    Public Class payment

        'Private Shared Sub Main(args As String())
        '    PostJson("https://api.amplifypay.com/merchant/transact", New template() With { _
        '        .merchantId = "NJEUEGZ22UI5MI7MZ4FQA", _
        '        .apiKey = "46f46a54-6865-4073-9bd8-e9d68fee0c3e", _
        '        .Amount = "200", _
        '        .customerEmail = "dipad10@rocketmail.com", _
        '        .planId = "200", _
        '        .paymentDescription = "BtcEx Bitcoin Payment", _
        '        .redirectUrl = "http://localhost:1118/private/payment_successful.aspx", _
        '        .transID = Guid.NewGuid.ToString _
        '    })


        'End Sub


        Public Shared Sub PostJson(uri As String, postParameters As template)
            Dim postData As String = JsonConvert.SerializeObject(postParameters)
            Dim bytes As Byte() = Encoding.UTF8.GetBytes(postData)
            Dim httpWebRequest = DirectCast(WebRequest.Create(uri), HttpWebRequest)
            httpWebRequest.Method = "POST"
            httpWebRequest.ContentLength = bytes.Length
            httpWebRequest.ContentType = "application/json"
            Using requestStream As Stream = httpWebRequest.GetRequestStream()
                requestStream.Write(bytes, 0, bytes.Count())
            End Using
            Dim httpWebResponse = DirectCast(httpWebRequest.GetResponse(), HttpWebResponse)
            If httpWebResponse.StatusCode <> HttpStatusCode.OK Then
                Dim message As String = [String].Format("POST failed. Received HTTP {0}", httpWebResponse.StatusCode)
                Throw New ApplicationException(message)
            End If
        End Sub

    End Class

    Public Class template

        Public Property merchantId() As String
            Get
                Return _merchantid
            End Get
            Set(value As String)
                _merchantid = value
            End Set
        End Property
        Private _merchantid As String
        Public Property apiKey() As String
            Get
                Return _apiKey
            End Get
            Set(value As String)
                _apiKey = value
            End Set
        End Property
        Private _apiKey As String

        Public Property transID() As String
            Get
                Return _transID
            End Get
            Set(value As String)
                _apiKey = value
            End Set
        End Property
        Private _transID As String

        Public Property customerEmail() As String
            Get
                Return _customerEmail
            End Get
            Set(value As String)
                _customerEmail = value
            End Set
        End Property
        Private _customerEmail As String

        Public Property Amount() As String
            Get
                Return _Amount
            End Get
            Set(value As String)
                _Amount = value
            End Set
        End Property
        Private _Amount As String

        Public Property redirectUrl() As String
            Get
                Return _redirectUrl
            End Get
            Set(value As String)
                _redirectUrl = value
            End Set
        End Property
        Private _redirectUrl As String

        Public Property paymentDescription() As String
            Get
                Return _paymentDescription
            End Get
            Set(value As String)
                _paymentDescription = value
            End Set
        End Property
        Private _paymentDescription As String


        Public Property planId() As String
            Get
                Return _planId
            End Get
            Set(value As String)
                _planId = value
            End Set
        End Property
        Private _planId As String

    End Class
End Namespace