﻿Imports System.Net
Imports System.Text
Imports System.Web.Mail
Imports System.Configuration
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Threading

Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports System.IO.Ports
Imports System.Net.Mail
'Imports CrystalDecisions.Shared
Imports System.Security.Cryptography
Imports System.IO
Imports System.Web.HttpContext
Public Module mod_main

    'reference this m_strconnString anywhere u want to call ur connection into d DB cos its now made public
    'Public m_strconnString As String = (New Lobitto.LobittoDataContext).Connection.ConnectionString
    Public settings As schoolsettings

    Public Enum ErrCodeEnum
        NO_ERROR = 0
        INVALID_GUID = 200
        INVALID_PASSWORD = 200
        FORBIDDEN = 407
        INVALID_RANGE = 408
        ACCOUNT_LOCKED = 400
        ACCOUNT_EXPIRED = 400
        GENERIC_ERROR = 100
    End Enum

    Public Class ResponseInfo
        Public ErrorCode As Integer
        Public ErrorMessage As String
        Public ExtraMessage As String
        Public TotalSuccess As Integer
        Public TotalFailure As Integer
        Public TotalCharged As Integer
        Public CurrentBalance As Integer
    End Class

#Region "GET FUNCTIONS"

    Public Function getQueryString(Optional ByVal q As NameValueCollection = Nothing, Optional ByVal SkipParam As String = "") As String
        If q Is Nothing Then q = My.Request.QueryString
        Dim query As String = "", skips() As String = Split(SkipParam, ",")

        For p As Integer = 0 To q.Count - 1
            If Not skips.Contains(q.GetKey(p)) Then
                query &= "&" & q.GetKey(p) & "=" & String.Join("", q.GetValues(p))
            End If
        Next

        If query.StartsWith("&") Then query = query.Remove(0, 1)
        Return query
    End Function

    Public Function IsValidEmail(ByVal strIn As String) As Boolean
        Try
            Return Regex.IsMatch(strIn, "^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function IsValidNigerianNo(ByVal MobileNo As String) As Boolean
        Return (MobileNo Like "23480########") Or (MobileNo Like "23470########") Or (MobileNo Like "080########") Or (MobileNo Like "070########") Or (MobileNo Like "090########") Or (MobileNo Like "081########")

    End Function
    Public Function DecodeFromBase64String(ByVal inputStr As String) As String
        Try
            If Len(inputStr) > 0 And (Len(inputStr) Mod 4) > 0 Then
                inputStr = inputStr & Left("====", (4 - (Len(inputStr) Mod 4)))
            End If
            ' Convert the binary input into Base64 UUEncoded output.
            Dim binaryData() As Byte = System.Convert.FromBase64String(inputStr)
            Return (New System.Text.UnicodeEncoding).GetString(binaryData)
        Catch exp As System.ArgumentNullException
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Structure schoolsettings
        Dim id As Integer
        Dim name As String
        Dim logo As String
        Dim address As String
        Dim website As String

    End Structure

    Public Sub Messagebox(ByVal Page As UI.Page, ByVal message As String)
        Page.Response.Cookies.Add(New HttpCookie("msgbox", message))
    End Sub
    Public Function getAdhocTable(ByVal strSQL As String, ByVal con As Data.Common.DbConnection) As Data.DataTable
        Dim cmd As New SqlCommand(strSQL, con)
        With cmd
            .CommandType = CommandType.Text

            Dim rdr As New SqlDataAdapter(cmd)
            Dim tbl As New DataTable("Table1")
            rdr.Fill(tbl) : Return tbl
        End With
    End Function
#End Region

#Region "CORE SYSTEM FUNCTIONS"
    Public Function _GetResponseStruct(ByVal ErrCode As ErrCodeEnum, Optional ByVal TotalSuccess As Integer = 0, _
     Optional ByVal TotalFailure As Integer = 0, Optional ByVal ErrorMsg As String = "", Optional ByVal ExtraMsg As String = "", Optional ByVal currentBalance As Integer = 0) As ResponseInfo
        Dim res As New ResponseInfo
        With res
            .ErrorCode = ErrCode
            .ErrorMessage = ErrorMsg
            .ExtraMessage = ExtraMsg
            .TotalSuccess = TotalSuccess
            .TotalFailure = TotalFailure
            .CurrentBalance = currentBalance
        End With
        Return res
    End Function


    Public Function Encrypt(ByVal clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function




    Public Function Decrypt(ByVal cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function


    Public Function PopulateBody(ByVal Username As String, ByVal Password As String, ByVal Email As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/WelcomeEmailTemplate.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Password}", Password)
        body = body.Replace("{Email}", Email)
     
        Return body
    End Function

    Public Function PopulateBodyactivation(ByVal Username As String, ByVal Url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateActivation.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Url}", Url)


        Return body


    End Function

    Public Function PopulateBodyforgotpwd(ByVal Username As String, ByVal password As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateforgotpwd.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Password}", password)

        Return body

    End Function
    Public Function PopulateBodyorderplaced(ByVal type As String, ByVal guid As String, ByVal username As String, ByVal btcamt As Double, ByVal nairaamt As Double, ByVal createdon As Date) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateorderplaced.html"))
        body = reader.ReadToEnd
        body = body.Replace("{type}", type)
        body = body.Replace("{guid}", guid)
        body = body.Replace("{btcamt}", btcamt)
        body = body.Replace("{nairaamt}", nairaamt)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{username}", username)

        Return body
    End Function

    Public Function PopulateBodyorderplacedadmin(ByVal type As String, ByVal guid As String, ByVal orderid As String, ByVal username As String, ByVal btcamt As Double, ByVal nairaamt As Double, ByVal createdon As Date) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateorderplacedadmin.html"))
        body = reader.ReadToEnd
        body = body.Replace("{type}", type)
        body = body.Replace("{guid}", guid)
        body = body.Replace("{orderid}", orderid)
        body = body.Replace("{btcamt}", btcamt)
        body = body.Replace("{nairaamt}", nairaamt)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{username}", username)

        Return body
    End Function

    Public Function PopulateBodyreplyadmin(ByVal TicketID As String, ByVal Creator As String, ByVal summary As String, ByVal message As String, ByVal priority As String, ByVal assignee As String, ByVal url As String, ByVal submittedon As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatereplyadmin.html"))
        body = reader.ReadToEnd
        body = body.Replace("{TicketID}", TicketID)
        body = body.Replace("{Creator}", Creator)
        body = body.Replace("{summary}", summary)
        body = body.Replace("{message}", message)
        body = body.Replace("{Priority}", priority)
        body = body.Replace("{Assignee}", assignee)
        body = body.Replace("{Url}", url)
        body = body.Replace("{submittedon}", submittedon)
        Return body
    End Function

    Public Function PopulateBodyreopened(ByVal TicketID As String, ByVal Creator As String, ByVal summary As String, ByVal details As String, ByVal priority As String, ByVal assignee As String, ByVal url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatereopened.html"))
        body = reader.ReadToEnd
        body = body.Replace("{TicketID}", TicketID)
        body = body.Replace("{Creator}", Creator)
        body = body.Replace("{summary}", summary)
        body = body.Replace("{Details}", details)
        body = body.Replace("{Priority}", priority)
        body = body.Replace("{Assignee}", assignee)
        body = body.Replace("{Url}", url)
        Return body
    End Function
    Public Function SendHtmlFormattedEmail(ByVal recepientEmail As String, ByVal cc As String, ByVal subject As String, ByVal body As String) As Boolean
        Try
            Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage
            mailMessage.From = New MailAddress(ConfigurationManager.AppSettings("UserName"), "BtcEx")
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail))
            If cc <> "" Then
                Dim CCId As String() = cc.Split(","c)

                For Each CCEmail As String In CCId
                    'Adding Multiple CC email Id
                    mailMessage.CC.Add(New MailAddress(CCEmail))


                Next
            Else
                'dont send cc email
            End If
          
            Dim smtp As SmtpClient = New SmtpClient
            smtp.Host = ConfigurationManager.AppSettings("Host")
            smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings("EnableSsl"))
            Dim NetworkCred As System.Net.NetworkCredential = New System.Net.NetworkCredential
            NetworkCred.UserName = ConfigurationManager.AppSettings("UserName")
            NetworkCred.Password = ConfigurationManager.AppSettings("Password")
            smtp.UseDefaultCredentials = True
            smtp.Credentials = NetworkCred
            smtp.Port = Integer.Parse(ConfigurationManager.AppSettings("Port"))
            smtp.Send(mailMessage)

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function FormatNaira(ByVal Num As String, Optional ByVal Sign As String = "&#8358;&nbsp;", Optional ByVal DecimalPlaces As Integer = 0) As String
        Dim N As String = Sign & " "
        Return (N & FormatNumber(Val(Num), DecimalPlaces, True, False, TriState.True)).Trim
    End Function
    'Public Function InsertActivity(Optional ByVal LogType As String = "", Optional ByVal Source As String = "", Optional ByVal Categories As String = "", Optional ByVal LogDetails As String = "", Optional ByVal SubmittedBy As String = "", Optional ByVal ticketid As String = "", Optional ByVal filepath As String = "", Optional ByVal filename As String = "") As Boolean

    '    Dim Rec As New GHD5.Activity

    '    Rec.LogType = LogType
    '    Rec.Source = Source
    '    Rec.Category = Categories
    '    Rec.Description = LogDetails
    '    Rec.SubmittedBy = SubmittedBy
    '    Rec.SubmittedOn = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
    '    Rec.UserID = ticketid
    '    Rec.FIlePath = filepath
    '    Rec.FileName = filename
    '    Dim res As ResponseInfo = (New cls_activity).Insert(Rec)
    '    If res.ErrorCode = 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If

    'End Function
    Public Function Updateautonumber(ByVal numbertype As String) As Boolean
        Try
            Dim A As New Cls_Autosettings
            Dim rec As naira.Autosetting = A.Getautonumbervalue(numbertype)
            rec.Nextvalue = rec.Nextvalue + 1
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
#End Region
End Module
