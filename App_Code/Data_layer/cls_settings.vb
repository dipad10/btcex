﻿Imports Microsoft.VisualBasic

Public Class cls_settings
    Dim DB As naira.nairaDataContext
    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As naira.Setting) As ResponseInfo
        Try
            DB.Settings.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As naira.Setting) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal sn As String) As naira.Setting
        Try
            Return (From R In DB.Settings Where R.settingid = sn).ToList()(0)
        Catch ex As Exception
            Return New naira.Setting

        End Try
    End Function



    Public Function SelectAllsettings() As List(Of naira.Setting)
        Try
            Return (From U In DB.Settings).ToList()
        Catch ex As Exception
            Return New List(Of naira.Setting)
        End Try
    End Function
End Class
