﻿Imports Microsoft.VisualBasic

Public Class cls_orders
    Dim DB As naira.nairaDataContext

    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As naira.Order) As ResponseInfo
        Try
            DB.Orders.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As naira.Order) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal R As naira.Order) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisorderid(ByVal ID As String) As naira.Order
        Try
            Return (From R In DB.Orders Where R.Orderid = ID).ToList()(0)
        Catch ex As Exception
            Return New naira.Order

        End Try
    End Function

    Public Function SelectThisorderlist(ByVal ID As String) As List(Of naira.Order)
        Try
            Return (From R In DB.Orders Where R.Orderid = ID).ToList
        Catch ex As Exception
            Return New List(Of naira.Order)

        End Try
    End Function

    Public Function SelectThisUsername(ByVal username As String) As naira.Order
        Try
            Return (From R In DB.Orders Where R.Username = username).ToList()(0)
        Catch ex As Exception
            Return New naira.Order

        End Try
    End Function

    Public Function SelectThisuserid(ByVal userid As String) As naira.Order
        Try
            Return (From R In DB.Orders Where R.Userid = userid).ToList()(0)
        Catch ex As Exception
            Return New naira.Order

        End Try
    End Function

    Public Function SelectThistransguid(ByVal guid As String) As naira.Order
        Try
            Return (From R In DB.Orders Where R.TransGUID = guid).ToList()(0)
        Catch ex As Exception
            Return New naira.Order

        End Try
    End Function
   
    Public Function SelectAllorders() As List(Of naira.Order)
        Try
            Return (From R In DB.Orders).ToList()
        Catch ex As Exception
            Return New List(Of naira.Order)
        End Try
    End Function
End Class
