﻿Imports Microsoft.VisualBasic

Public Class cls_blog
    Dim DB As naira.nairaDataContext
    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As naira.blog) As ResponseInfo
        Try
            DB.blogs.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal G As naira.blog) As ResponseInfo
        Try
            DB.blogs.DeleteOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As naira.blog) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal id As String) As naira.blog
        Try
            Return (From R In DB.blogs Where R.id = id).ToList()(0)
        Catch ex As Exception
            Return New naira.blog

        End Try
    End Function



    Public Function SelectAllsettings() As List(Of naira.blog)
        Try
            Return (From U In DB.blogs).ToList()
        Catch ex As Exception
            Return New List(Of naira.blog)
        End Try
    End Function
End Class
