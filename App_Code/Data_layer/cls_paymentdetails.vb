﻿Imports Microsoft.VisualBasic

Public Class cls_paymentdetails
    Dim DB As naira.nairaDataContext

    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As naira.PaymentDetail) As ResponseInfo
        Try
            DB.PaymentDetails.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As naira.PaymentDetail) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal R As naira.PaymentDetail) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThispaymentid(ByVal ID As String) As naira.PaymentDetail
        Try
            Return (From R In DB.PaymentDetails Where R.PaymentID = ID).ToList()(0)
        Catch ex As Exception
            Return New naira.PaymentDetail

        End Try
    End Function

    Public Function SelectThisorderlist(ByVal ID As String) As List(Of naira.PaymentDetail)
        Try
            Return (From R In DB.PaymentDetails Where R.OrderID = ID).ToList
        Catch ex As Exception
            Return New List(Of naira.PaymentDetail)

        End Try
    End Function

    Public Function SelectThisorderid(ByVal orderid As String) As naira.PaymentDetail
        Try
            Return (From R In DB.PaymentDetails Where R.OrderID = orderid).ToList()(0)
        Catch ex As Exception
            Return New naira.PaymentDetail

        End Try
    End Function

    Public Function SelectThisuserid(ByVal userid As String) As naira.PaymentDetail
        Try
            Return (From R In DB.PaymentDetails Where R.UserID = userid).ToList()(0)
        Catch ex As Exception
            Return New naira.PaymentDetail

        End Try
    End Function

  

    Public Function SelectAllpaymentdetails() As List(Of naira.PaymentDetail)
        Try
            Return (From R In DB.PaymentDetails).ToList()
        Catch ex As Exception
            Return New List(Of naira.PaymentDetail)
        End Try
    End Function
End Class
