﻿Imports Microsoft.VisualBasic

Public Class cls_announcement
    Dim DB As naira.nairaDataContext
    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As naira.Announcement) As ResponseInfo
        Try
            DB.Announcements.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As naira.Announcement) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal sn As String) As naira.Announcement
        Try
            Return (From R In DB.Announcements Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New naira.Announcement

        End Try
    End Function



    Public Function SelectAllsettings() As List(Of naira.Announcement)
        Try
            Return (From U In DB.Announcements).ToList()
        Catch ex As Exception
            Return New List(Of naira.Announcement)
        End Try
    End Function
End Class
