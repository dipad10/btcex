﻿Imports Microsoft.VisualBasic

Public Class cls_users
    Dim DB As naira.nairaDataContext

    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As naira.User) As ResponseInfo
        Try
            DB.Users.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As naira.User) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal R As naira.User) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisUserID(ByVal ID As String) As naira.User
        Try
            Return (From R In DB.users Where R.UserID = ID).ToList()(0)
        Catch ex As Exception
            Return New naira.User

        End Try
    End Function

    Public Function SelectThisguid(ByVal guid As String) As naira.User
        Try
            Return (From R In DB.Users Where R.ActivationCode = guid).ToList()(0)
        Catch ex As Exception
            Return New naira.User

        End Try
    End Function

    Public Function SelectThisUsername(ByVal username As String) As naira.User
        Try
            Return (From R In DB.users Where R.UserName = username).ToList()(0)
        Catch ex As Exception
            Return New naira.User

        End Try
    End Function

    Public Function SelectThisemail(ByVal email As String) As naira.User
        Try
            Return (From R In DB.Users Where R.Email = email).ToList()(0)
        Catch ex As Exception
            Return New naira.User

        End Try
    End Function
    Public Function SelectThisemailist(ByVal email As String) As List(Of naira.User)
        Try
            Return (From R In DB.Users Where R.Email = email).ToList()
        Catch ex As Exception
            Return New List(Of naira.User)

        End Try
    End Function

    Public Function Authenticatelogin(ByVal email As String, ByVal password As String) As List(Of naira.User)
        Try
            Return (From R In DB.Users Where R.Email = email And R.Password = password And R.Active = 1).ToList()
        Catch ex As Exception
            Return New List(Of naira.User)

        End Try
    End Function
    Public Function SelectAllUsers() As List(Of naira.User)
        Try
            Return (From R In DB.users).ToList()
        Catch ex As Exception
            Return New List(Of naira.User)
        End Try
    End Function
End Class
