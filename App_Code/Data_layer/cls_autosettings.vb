﻿Imports Microsoft.VisualBasic

Public Class cls_autosettings
    Dim DB As naira.nairaDataContext
    Public Sub New(Optional ByVal Context As naira.nairaDataContext = Nothing)
        If Context Is Nothing Then
            DB = New naira.nairaDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As naira.Autosetting) As ResponseInfo
        Try
            DB.Autosettings.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As naira.Autosetting) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal SN As String) As naira.Autosetting
        Try
            Return (From R In DB.Autosettings Where R.SN = SN).ToList()(0)
        Catch ex As Exception
            Return New naira.Autosetting

        End Try
    End Function



    Public Function Getautonumbervalue(ByVal numbertype As String) As naira.Autosetting
        Try
            Return (From R In DB.Autosettings Where R.NumberType = numbertype).ToList()(0)
        Catch ex As Exception
            Return New naira.Autosetting

        End Try
    End Function


    Public Function SelectAllsettings() As List(Of naira.Autosetting)
        Try
            Return (From U In DB.Autosettings).ToList()
        Catch ex As Exception
            Return New List(Of naira.Autosetting)
        End Try
    End Function
End Class
